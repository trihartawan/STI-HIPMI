$(function() {
	"use strict";
	$(".left-sidebar").hover(
		function() {
			$(".navbar-header").addClass("expand-logo");
		},
		function() {
			$(".navbar-header").removeClass("expand-logo");
		}
	);
	$(".nav-toggler").on('click', function() {
		$("#main-wrapper").toggleClass("show-sidebar");
		$(".nav-toggler i").toggleClass("mdi-menu");
	});
	$('.message-center, .customizer-body, .scrollable').perfectScrollbar({ 
		wheelPropagation: !0 
	});
	$(".show-left-part").on('click', function() {
		$('.left-part').toggleClass('show-panel');
		$('.show-left-part').toggleClass('mdi-menu');
	});
	$("body, .page-wrapper").trigger("resize");
	$(".page-wrapper").delay(20).show();
});

// tooltip
$('[data-toggle="tooltip"]').tooltip();

// popover
$('[data-toggle="popover"]').popover();

// datatable
$('.datatable').DataTable();

// selectpicker
$('.selectpicker').chosen({});