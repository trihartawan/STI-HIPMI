<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <title><?php echo $heading ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        html, body {
            height: 100%;
        }

        h1 {
            color: #36bea6;
            font-size: 45px!important;
        }

        /* CSS only for examples not required for centering */
        .container {
            height: 100%;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row h-100">
        <div class="col-sm-12 align-self-center">
            <div class="text-center mx-auto">
                <h1>Whoops! Something is Broken</h1>
                <h4><?php echo $heading ?></h4>
                <div>
                    <?php echo $message; ?>
                </div>
                <a href="/">
                    Back to Homepage
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>