<script src="{{ config('bootstrap', 'assets_back') }}js/jquery.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/popper.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/bootstrap.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/app.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/app.init.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/perfect-scrollbar.jquery.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/waves.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/sidebarmenu.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/chosen.jquery.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/datatables.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/sweetalert2.all.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/styles.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/stifw.js"></script>


<!-- <script src="{{ config('bootstrap', 'assets_back') }}ckeditor/adapters/jquery.js""></script> -->
<script>var APP_NAME = "{{ WEBSITE_NAME }}"</script>
@yield('custom_js')
@if(flash('MSG_SUCCESS'))
<script>
    swal({
        title: 'Aksi Berhasil',
        html: '{{ flash('MSG_SUCCESS') }}',
        type: 'success',
        confirmButtonClass: 'btn btn-primary',
        timer: 2500
    });
</script>
@elseif(flash('MSG_FAILED'))
<script>
    swal({
        title: 'Aksi Berhasil',
        html: '{{ flash('MSG_FAILED') }}',
        type: 'error',
        confirmButtonClass: 'btn btn-primary',
        timer: 2500
    });
</script>
@endif

<!-- <script type="text/javascript">
        $('textarea.texteditor').ckeditor();
</script> -->