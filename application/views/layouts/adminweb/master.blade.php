<!DOCTYPE html>
<html dir="ltr" lang="id">
<head>
    @include('layouts.adminweb.header')
</head>
<body>
    <div id="main-wrapper">
        <!-- TOPBAR -->
        @include('layouts.adminweb.topbar')
        <!-- END TOPBAR -->
        <!-- SIDEBAR -->
        @include('layouts.adminweb.sidebar')
        <!-- END SIDEBAR -->
        <div class="page-wrapper">
            @if(isset($title))
            <!-- BREADCRUMB -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 align-self-center">
                        <h4 class="page-title">
                            <i class="@yield('header_icon') fa-fw"></i>
                            {{ $title }}
                        </h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END BREADCRUMB -->
            @endif
            <!-- CONTENTS -->
            <div class="container-fluid">
                @yield('content')
            </div>
            <!-- END CONTENTS -->
            <!-- FOOTER -->
            <footer class="footer">
                <span>&COPY; {{ Carbon\Carbon::now()->year }} - {{ anchor(base_url(), COMPANY_NAME, 'class="text-primary"') }}</span>
                <span class="text-muted float-right"><i class="fa fa-line-chart fa-fw"></i> {elapsed_time}s</span>
            </footer>
            <!-- END FOOTER -->
        </div>
        @yield('modal')
    </div>
    <!-- SCRIPT -->
    @include('layouts.adminweb.footer')
    <!-- END SCRIPT -->
</body>
</html>
