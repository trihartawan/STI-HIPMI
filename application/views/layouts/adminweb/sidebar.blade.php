<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li>
                    <div class="user-profile d-flex no-block dropdown m-t-20">
                        <div class="user-pic"><img src="{{ session(BACKGROUND_SESSION)['user_picture'] }}" alt="users" class="rounded-circle" width="40" /></div>
                        <div class="user-content hide-menu m-l-10">
                            <a href="#)" class="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <h5 class="m-b-0 user-name font-medium">{{ session(BACKGROUND_SESSION)['user_fullname'] }}</h5>
                                <span class="op-5 user-email">{{ session(BACKGROUND_SESSION)['roles_nama'] }}</span>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="sidebar-item">
                    <a href="{{ base_url('beranda') }}" class="sidebar-link"><i class="fa fa-home"></i><span class="hide-menu">Beranda</span></a>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-database"></i><span class="hide-menu">Data Master</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="javascript:void(0)" class="sidebar-link"><i class="fa fa-arrow-circle-right"></i><span class="hide-menu">Perusaaan</span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="javascript:void(0)" class="sidebar-link"><i class="fa fa-arrow-circle-right"></i><span class="hide-menu">Cabang</span></a>
                        </li>
                         <li class="sidebar-item">
                            <a href="javascript:void(0)" class="sidebar-link"><i class="fa fa-arrow-circle-right"></i><span class="hide-menu">Departemen</span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{ base_url('master/devisi') }}" class="sidebar-link"><i class="fa fa-arrow-circle-right"></i><span class="hide-menu">Divisi</span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="javascript:void(0)" class="sidebar-link"><i class="fa fa-arrow-circle-right"></i><span class="hide-menu">Karyawan</span></a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fa fa-cogs"></i><span class="hide-menu">Management</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="{{ base_url('mgmt/role') }}" class="sidebar-link"><i class="fa fa-arrow-circle-right"></i><span class="hide-menu">Role</span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="javascript:void(0)" class="sidebar-link"><i class="fa fa-arrow-circle-right"></i><span class="hide-menu">Pengguna</span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="javascript:void(0)" class="sidebar-link"><i class="fa fa-arrow-circle-right"></i><span class="hide-menu">Modul</span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="javascript:void(0)" class="sidebar-link"><i class="fa fa-arrow-circle-right"></i><span class="hide-menu">Perijinan Sistem</span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="javascript:void(0)" class="sidebar-link"><i class="fa fa-arrow-circle-right"></i><span class="hide-menu">Aplikasi</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>