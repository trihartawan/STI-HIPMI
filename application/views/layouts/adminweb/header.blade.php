<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{ (isset($title)) ? $title." - " : null; }}{{ WEBSITE_NAME }}</title>
<link rel="shortcut icon" href="{{ config('bootstrap', 'assets_back') }}favicon.ico" />
<link rel="apple-touch-icon" href="{{ config('bootstrap', 'assets_back') }}img/logo/stifw.png" />
<link rel="apple-touch-icon-precomposed" href="{{ config('bootstrap', 'assets_back') }}img/logo/stifw.png" />
<!-- STYLE -->
<link href="{{ config('bootstrap', 'assets_back') }}css/bootstrap.min.css" rel="stylesheet">
<link href="{{ config('bootstrap', 'assets_back') }}css/animate.css" rel="stylesheet">
<link href="{{ config('bootstrap', 'assets_back') }}css/waves.css" rel="stylesheet">
<link href="{{ config('bootstrap', 'assets_back') }}css/perfect-scrollbar.css" rel="stylesheet">
<link href="{{ config('bootstrap', 'assets_back') }}css/dataTables.bootstrap4.css" rel="stylesheet">
<link href="{{ config('bootstrap', 'assets_back') }}css/chosen.min.css" rel="stylesheet">
<link href="{{ config('bootstrap', 'assets_back') }}css/materialdesignicons.min.css" rel="stylesheet">
<link href="{{ config('bootstrap', 'assets_back') }}css/fontawesome.min.css" rel="stylesheet">
<link href="{{ config('bootstrap', 'assets_back') }}css/sweetalert2.min.css" rel="stylesheet">
<link href="{{ config('bootstrap', 'assets_back') }}css/neo.css" rel="stylesheet">
<link href="{{ config('bootstrap', 'assets_back') }}css/samples.css" rel="stylesheet">
<link href="{{ config('bootstrap', 'assets_back') }}css/style.min.css" rel="stylesheet">

<!-- END STYLE -->
@yield('custom_css')
