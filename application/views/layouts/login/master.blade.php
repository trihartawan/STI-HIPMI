<!DOCTYPE html>
<html dir="ltr" lang="id">
<head>
    @include('layouts.adminweb.header')
</head>
<body>
    <div id="main-wrapper">
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url({{ config('bootstrap', 'assets_back') }}img/bgs/bg_login.jpg) no-repeat center center;">
            <div class="auth-box">
                <div class="row mb-4 p-t-20">
                    <div class="col-lg-4 offset-lg-1 text-right">
                        <img src="{{ config('bootstrap', 'assets_back') }}img/logo/stifw.png" height="80" alt="logo" />
                    </div>
                    <div class="col-lg-6 m-t-15">
                        <h3 class="m-0">{{ WEBSITE_NAME }}</h3>
                        <span>A CMS Made for STI</span>
                    </div>
                </div>
                <!-- CONTENT -->
                @yield('content')
                <!-- END CONTENT -->
            </div>
        </div>
    </div>
    <!-- SCRIPT -->
    @include('layouts.login.footer')
    <!-- END SCRIPT -->
</body>
</html>
