<script src="{{ config('bootstrap', 'assets_back') }}js/jquery.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/popper.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/bootstrap.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/sweetalert2.all.min.js"></script>
@yield('custom_js')
@if(flash('MSG_SUCCESS'))
<script>
    swal({
        title: 'Aksi Berhasil',
        html: '{{ flash('MSG_SUCCESS') }}',
        type: 'success',
        confirmButtonClass: 'btn btn-primary',
        timer: 2500
    });
</script>
@elseif(flash('MSG_FAILED'))
<script>
    swal({
        title: 'Aksi Berhasil',
        html: '{{ flash('MSG_FAILED') }}',
        type: 'error',
        confirmButtonClass: 'btn btn-primary',
        timer: 2500
    });
</script>
@endif