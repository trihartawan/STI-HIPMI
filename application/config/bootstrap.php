<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['assets_back']  = base_url('/assets/backend/');
$config['assets_front'] = base_url('/assets/landing/');