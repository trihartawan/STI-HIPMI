<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'mgmt/beranda';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['beranda']       = 'mgmt/beranda';
$route['profil']        = 'mgmt/profile/index';
$route['profil/(:any)'] = 'mgmt/profile/index/$1';
$route['login']         = 'mgmt/login';
$route['forgot']        = 'mgmt/login/lupa_password';
$route['logout']        = 'mgmt/login/logout';