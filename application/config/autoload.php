<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages']   = array();
$autoload['libraries']  = array('database', 'session', 'email', 'slice', 'user_agent', 'form_validation', 'table');
$autoload['drivers']    = array();
$autoload['helper']     = array('url', 'security', 'string', 'cookie', 'sti', 'text');
$autoload['config']     = array();
$autoload['language']   = array();
$autoload['model']      = array('M_login', 'M_user_table', 'M_session', 'M_role', 'M_perusahaan', 'M_cabang', 'M_karyawan');
