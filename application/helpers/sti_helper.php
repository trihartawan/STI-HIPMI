<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dapatkan item dari file config
 * 
 * @param	string $name nama file config
 * @param	string $item dapatkan value dari nama item
 * @return	mixed
 */
if ( ! function_exists('config')){
	function config($name = null, $item = null){
		$output = false;

		// load instance CI
		$CI =& get_instance();
		$CI->load->config($name);

		return $CI->config->item($item);
	}
}

/**
 * Dapatkan item dari method POST
 * 
 * @param	string $name nama item yang ada di POST request
 * @return	mixed
 */
if ( ! function_exists('post')){
    function post($name = null){
        $CI =& get_instance();
        
        // get application's setting
        return $CI->input->post($name);
    }
}

/**
 * Dapatkan item dari method GET
 * 
 * @param	string $name nama item yang ada di GET request
 * @return	mixed
 */
if ( ! function_exists('get')){
    function get($name = null){
        $CI =& get_instance();
        
        // get application's setting
        return $CI->input->get($name);
    }
}

/**
 * Set/Get session user
 * 
 * @param	string $context nama item dalam session
 * @param	bool $is_temp apakah session ini bersifat sementara
 * @return	mixed
 */
if ( ! function_exists('session')){
    function session($context = null, $is_temp = false){
        $CI =& get_instance();
        
        // bila array maka, masukkan konten array ke dalam session
        if(is_array($context)){
            foreach($context as $key => $row){
                // bila data temporary
                if($is_temp){
                    $CI->session->set_tempdata($key, $row, 600);
                }else{
                    $CI->session->set_userdata($key, $row);
                }
                $output = true;
            }
        }else{
            // get result from session
            if($is_temp){
                $output = $CI->session->tempdata($context);                
            }else{
                $output = $CI->session->userdata($context);
            }             
        }

        return $output;
    }
}

/**
 * Set/Get session flash user
 * 
 * @param	string $context nama item dalam session flash
 * @return	mixed
 */
if ( ! function_exists('flash')){
    function flash($context = null){
        $CI =& get_instance();
        
        // bila array maka, masukkan konten array ke dalam session
        if(is_array($context)){
            foreach($context as $key => $row){
                $CI->session->set_flashdata($key, $row);
            }
            $output = true;            
        }else{
            // get result from session
            $output = $CI->session->flashdata($context);
        }

        return $output;
    }
}

/**
 * Kirim email melalui konfigurasi yang s=tersedia
 * 
 * @param	string $subject judul email
 * @param	string $message pesan yang ingin dikirimkan
 * @param	string $sender alamat email pengirim
 * @param	string $respondent alamat email penerima
 * @return	mixed
 */
if ( ! function_exists('send_email')){
    function send_email($subject = null, $message, $sender, $respondent){
        
        $output = false;

        // get CI instance
        $CI =& get_instance();
        
        // load config for email library
        $config = Array(
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.mailtrap.io',
            'smtp_port' => 2525,
            'smtp_user' => '6109f0d8e0ec1b',
            'smtp_pass' => '24438dd1885ce7',
            'crlf'      => "\r\n",
            'newline'   => "\r\n"
        );
        
        $CI->load->library('email', $config);

        // email operation
        $CI->email->set_mailtype("html");        
        $CI->email->from((isset($sender)) ? $sender : "admin@sti.com");
        $CI->email->to($respondent);
        $CI->email->subject($subject);
        $CI->email->message($message);

        if($CI->email->send()){
            $output = true;
        }else{
            show_error($CI->email->print_debugger());            
        }

        return $output;
    }
}

/**
 * Validasi untuk form yang disubmit
 * 
 * @param	array string $options rule validasi
 * @return	bool
 */
if ( ! function_exists('validation')){
    function validation($options){
        $CI =& get_instance();
        
        // cek apakah options itu array atau bukan
        if(is_array($options)){
            // bila jumlah options ini lebih dari 1 maka looping array childnya
            if(count($options[0]) > 1){
                foreach($options as $row){
                    if(is_array($row)){
                        // cek apakah row itu array, bila array optionnya lebih dari satu
                        $name       = (isset($row[0])) ? $row[0] : die("ERROR_ITEM_NAME_NOT_FOUND");
                        $real_name  = (isset($row[1])) ? $row[1] : $name;
                        $rule       = (isset($row[2])) ? $row[2] : NULL;
                        $CI->form_validation->set_rules($name, $real_name, $rule);
                    }
                }
            }else{
                // bila bukan langsung masukkan dalam rule
                $name       = (isset($options[0])) ? $options[0] : die("ERROR_ITEM_NAME_NOT_FOUND");
                $real_name  = (isset($options[1])) ? $options[1] : $name;
                $rule       = (isset($options[2])) ? $options[2] : NULL;
                $CI->form_validation->set_rules($name, $real_name, $rule);
            }

            // running validation
            return $CI->form_validation->run();
        }
    }
}

// TODO: JAVDOC
if (!function_exists('validation_errors_array')) {

   function validation_errors_array($prefix = '', $suffix = '') {
      if (FALSE === ($OBJ = & _get_validation_object())) {
        return '';
      }

      return $OBJ->error_array($prefix, $suffix);
   }
}

// TODO: JAVDOC
if (!function_exists('encrypt_decrypt')) {

    function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'This is my secret key';
        $secret_iv = 'This is my secret iv';
        // hash
        $key = hash('sha256', $secret_key);
        
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
}
