@extends('layouts.adminweb.master')

@section('header_icon',' fa fa-id-card-o')

@section('content')
<div class="text-right" style="padding-bottom: 20px">
	<button type="button" class="btn btn-primary create-action"><i class="fa fa-plus"></i> Tambah Data</button>
	<button type="button" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus Data</button>
</div>
<div class="table-primary">
  <table class="table table-striped table-bordered" id="datatables">
    <thead>
      <tr>
        <th class="text-center" style="width: 7%">No</th>
        <th>Nama Devisi</th>
        <th style="width: 12%">Action</th>
      </tr>
    </thead>
    <tbody>
    	<!-- TODO : action dropdown, check box -->
      <tr class="odd gradeX">
        <td class="text-center" style="vertical-align: middle;">1</td>
        <td class="center"><b>General</b><br>Alias : Div. General</td>
        <td class="center" style="vertical-align: middle;">
        	<button type="button" class="btn btn-sm btn-warning update-action"><i class="fa fa-pencil-square-o"></i></button>
        	<button type="button" class="btn btn-sm btn-danger delete-action" data-id="1"><i class="fa fa-trash"></i></button>
        </td>
      </tr>
    </tbody>
  </table>
</div>
@endsection

@section('modal')
<div class="modal fade" id="modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<form class="col-md-8" id="form">
            <input type="text" name="aksi" value="" hidden="">
	        	<div class="form-group row">
              <div id="error" class="col-md-12"></div>
	        		<label class="col-md-3 text-right">Code</label>
	        		<label class="col-md-1 text-right">:</label>	
	        		<div class="col-md-8">
	        			<input class=" form-control" type="text" name="code">
	        		</div>
	        	</div>
	        	<div class="form-group row">
              <div id="error" class="col-md-12"></div>
	        		<label class="col-md-3 text-right">Nama Devisi</label>
	        		<label class="col-md-1 text-right">:</label>	
	        		<div class="col-md-8">
	        			<input class=" form-control" type="text" name="nama">
	        		</div>
	        	</div>
            <div class="form-group row">
              <label class="col-md-3 text-right">Parent</label>
              <label class="col-md-1 text-right">:</label>  
              <div class="col-md-8">
                <select class="form-control select2-example" style="width: 100%" data-allow-clear="true">
                <option></option>
                <option value="AK">Alaska</option>
                <option value="HI">Hawaii</option>
                <option value="CA">California</option>
                <option value="NV">Nevada</option>
                <option value="OR">Oregon</option>
                <option value="WA">Washington</option>
                <option value="AZ">Arizona</option>
                <option value="CO">Colorado</option>
                <option value="ID">Idaho</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NM">New Mexico</option>
                <option value="ND">North Dakota</option>
                <option value="UT">Utah</option>
                <option value="WY">Wyoming</option>
                <option value="AL">Alabama</option>
                <option value="AR">Arkansas</option>
                </select>
              </div>
            </div>            
	        </form>
	        <div class="col-md-4">
	           <p><b>Petunjuk :</b></p>
	            <span>Lengkapi form yang diperlukan di sebelah untuk membuat data baru</span>
	        </div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary save-action">Save</button>
      </div>
    </div>

  </div>
</div>
@endsection

@section('custom_css')
<style>
  .select2-dropdown{
  z-index: 9999999999999;
}
</style>
@endsection

@section('custom_js')
  <script>
    // -------------------------------------------------------------------------
    // Initialize Select2

    $(function() {
      $('.select2-example').select2({
        placeholder: 'Select value',
        
      });
    });
    
    // -------------------------------------------------------------------------
    // Initialize DataTables
    $(function() {
      $('#datatables').dataTable();
      $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
    });

    glAjax('#form','{{ base_url() }}master/devisi/','#modal','Devisi');
  </script>
@endsection
