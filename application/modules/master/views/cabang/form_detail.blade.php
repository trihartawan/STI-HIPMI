<div class="row">
    <div class="col-lg-4">
        <p><strong>Petunjuk :</strong></p>
        <span>
            Lengkapilah form yang tersedia untuk dapat menyimpan data.
        </span>
    </div>
    <div class="col-lg-8">
        <form class="form-horizontal module-form" role="edit">

            <div class="form-group row mb-0">
                <h3 class="col-sm-12">{{ $perusahaan }}</h3>
            </div>
            <div class="form-group row">
                <h5 class="col-sm-12 text-info">
                    {{ ($data_db[0]->is_hq == 1) ? "Headquerter" : "Kantor Cabang"; }}
                </h5>
                <hr class="col-sm-11">
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Kode</label>
                <label class="col-sm-10 text-left control-label col-form-label">{{ $data_db[0]->kode }}</label>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Nama Cabang</label>
                <label class="col-sm-10 text-left control-label col-form-label">{{ $data_db[0]->nama }}</label>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Alamat</label>
                <label class="col-sm-10 text-left control-label col-form-label">{{ $data_db[0]->alamat }}</label>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">No. Telpon</label>
                <label class="col-sm-10 text-left control-label col-form-label">{{ $data_db[0]->telpon }}</label>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">No. Fax</label>
                <label class="col-sm-10 text-left control-label col-form-label">{{ $data_db[0]->fax }}</label>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Negara</label>
                <label class="col-sm-10 text-left control-label col-form-label">{{ $data_db[0]->negara }}</label>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Kodepos</label>
                <label class="col-sm-10 text-left control-label col-form-label">{{ $data_db[0]->kodepos }}</label>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">NPWP</label>
                <label class="col-sm-10 text-left control-label col-form-label">{{ $data_db[0]->npwp }}</label>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">No. Rekening</label>
                <label class="col-sm-10 text-left control-label col-form-label">{{ $data_db[0]->rekening }}</label>
            </div>
        </form>
    </div>
</div>