<div class="row">
    <div class="col-lg-4">
        <p><strong>Petunjuk :</strong></p>
        <span>
            Lengkapilah form yang tersedia untuk dapat menyimpan data.
        </span>
    </div>
    <div class="col-lg-8">
        <form class="form-horizontal module-form" role="edit">
        <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Perusahaan</label>
                <div class="col-sm-10">
                    {{ $perusahaan }}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Kode</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="kode" maxlength="40" placeholder="Masukkan Data" value="{{ $data_db[0]->kode }}" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Nama Cabang</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="nama" placeholder="Masukkan Data" value="{{ $data_db[0]->nama }}" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Alamat</label>
                <div class="col-sm-10">
                    <textarea name="alamat" class="form-control" rows="4" placeholder="Masukkan Data" required>{{ $data_db[0]->alamat }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">No. Telpon</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="telpon" placeholder="Masukkan Data" maxlength="20" value="{{ $data_db[0]->telpon }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">No. Fax</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="fax" placeholder="Masukkan Data" maxlength="20" value="{{ $data_db[0]->fax }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Negara</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="negara" placeholder="Masukkan Data" value="{{ $data_db[0]->negara }}" readonly required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Kodepos</label>
                <div class="col-sm-10">
                    <input type="number" maxlength="6" class="form-control" name="kodepos" placeholder="Masukkan Data" value="{{ $data_db[0]->kodepos }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">NPWP</label>
                <div class="col-sm-10">
                    <input type="text" maxlength="40" class="form-control" name="npwp" placeholder="Masukkan Data" value="{{ $data_db[0]->npwp }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">No. Rekening</label>
                <div class="col-sm-10">
                    <input type="text" maxlength="60" class="form-control" name="rekening" placeholder="Masukkan Data" value="{{ $data_db[0]->rekening }}">
                </div>
            </div>
            <div class="custom-control custom-checkbox mr-sm-2 m-b-15">
                <input type="checkbox" class="custom-control-input" name="is_hq" id="cbIsHQ" value="check" {{ ($data_db[0]->is_hq == 1) ? 'checked' : null; }}>
                <label class="custom-control-label" for="cbIsHQ">Apakah Ini Cabang Pusat?</label>
            </div>
            <div class="form-group m-b-0 text-right">
                <button type="button" class="btn btn-success waves-effect waves-light action-submit">Simpan</button>
            </div>
        </form>
    </div>
</div>