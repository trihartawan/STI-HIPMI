<div class="row">
    <div class="col-lg-12"> 
     <p><strong>Petunjuk :</strong></p>
        <span>
            Silahkan tambahkan agenda yang anda inginkan.
        </span>
    </div>
    <div class="col-lg-12">
    <hr>    
        <form class="form-horizontal module-form" role="create">
            <div class="row">   
                <div class="col-lg-7"> 
                    <div class="form-group">
                        <label>Judul Konten</label>
                            <input type="text" class="form-control" name="judul" placeholder="Masukkan judul" required>
                    </div>
                    
                    <div class="row">
                         <div class="col-lg-6">  
                             <div class="form-group">
                            <label class="control-label col-form-label">Kategori Konten</label>
                                <select name="kategori" class="form-control" id="kategori">
                                        <option value="">--pilih--</option>
                                        <option value="">kategori</option>
                                        <option value="">kategori</option>
                                </select>
                            </div>
                              <div class="form-group">
                                <label class="control-label col-form-label">Lokasi</label>
                                <input type="text" class="form-control" name="lokasi" placeholder="Lokasi" required>
                         </div>
                        </div>
                        <div class="col-lg-6">  
                             <div class="form-group">
                             <label class="control-label col-form-label">Bahasa Konten</label>
                                <select name="bahasa" class="form-control" id="bahasa">
                                    <option value="">--pilih--</option>
                                    <option value="">Indonesia</option>
                                    <option value="">English</option>
                                 </select>
                             </div> 
                             <div class="form-group">
                                <label class="control-label col-form-label">Penyelenggara</label>
                                <input type="text" class="form-control" name="penyelenggara" placeholder="Masukkan Data" required>
                            </div>
                        </div>   
                    </div>
                   
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-5">  
                         <div class="form-group">
                        <label class="control-label col-form-label">Tanggal Pelaksana</label>
                            <input type="date" name="tanggal_pelaksanaan" class="form-control">
                        </div>
                        <div class="form-group">
                        <label class="control-label col-form-label">Gambar Poster</label>
                               <input type="file" name="gambar" class="form-control">
                        </div>
                        <div class="form-group">
                        <label class="control-label col-form-label">Eksternal Link</label>
                            <input type="text" class="form-control" name="link" placeholder="http://">
                            <p>Apabila agenda ini memiliki halaman terpisah di luar web ini</p>
                        </div>

                </div>
            </div>
                 <hr>   
            <label for="">Detail Maps</label>
            <div id="map" style="width: 100%;height: 500px"></div>
            <br>    
            <div class="form-group m-b-0 text-right">
                <button type="button" class="btn btn-success waves-effect waves-light action-submit">Simpan</button>
            </div>
        </form>
    </div>
</div>

  <script>

      function initMap() {
        var myLatLng = {lat: -2.279866, lng: 117.369878};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
      }
    </script>
    
       

     <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBaRBpqdzadDv2u8Gb1ig7QrP-dQyNQNLI&callback=initMap">
    </script>