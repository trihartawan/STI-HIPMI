<div class="row">
    <div class="col-lg-4">
        <p><strong>Petunjuk :</strong></p>
        <span>
            Lengkapilah form yang tersedia untuk dapat menyimpan data.
        </span>
    </div>
    <div class="col-lg-8">
        <form class="form-horizontal module-form" role="create">
            <div class="form-group row">
                <label class="col-sm-3 text-left control-label col-form-label">Nama Departemen</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="nama" placeholder="Masukkan Data" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 text-left control-label col-form-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" name="email" placeholder="someone@example.com" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 text-left control-label col-form-label">Website</label>
                <div class="col-sm-9">
                    <input type="url" class="form-control" name="website" placeholder="http://www.example.com">
                </div>
            </div>
            <div class="form-group m-b-0 text-right">
                <button type="button" class="btn btn-success waves-effect waves-light action-submit">Simpan</button>
            </div>
        </form>
    </div>
</div>