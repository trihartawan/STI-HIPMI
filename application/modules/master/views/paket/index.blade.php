@extends('layouts.adminweb.master')

@section('header_icon',' fa fa-cube')

@section('content')
<ul class="nav nav-tabs">
              <li class="active">
                <a href="#paket" data-toggle="tab">
                  Paket
                </a>
              </li>
              <li>
                <a href="#lisensi" data-toggle="tab">
                 Lisensi
                </a>
              </li>
              <li>
                <a href="#order" data-toggle="tab">
                 Riwayat Order
                </a>
              </li>
              <li>
                <a href="#deposit" data-toggle="tab">
                 Deposit
                </a>
              </li>
            </ul>

            <div class="tab-content tab-content-bordered">
              <div class="tab-pane fade in active" id="paket">
                <div class="table-primary">
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center" style="width: 7%">No</th>
                        <th>Produk</th>
                        <th>Item</th>
                        <th>Qty</th>
                        <th>Aktif s.d</th>
                        <th style="width: 12%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- TODO : action dropdown, check box -->
                      <tr class="odd gradeX">
                        <td class="text-center">1</td>
                        <td><a href="">Platinum 1 bulan Starter(Halal Mart Indonesia)</a></td>
                        <td class="center">
                          <ul style="list-style-type: disc;padding-left:10px">
                              <li><strong>Max. User</strong></li>
                              <li><strong>Max. Produk</strong></li>
                              <li><strong>Max. Cabang</strong></li>
                              <li><strong>Max. Custumer</strong></li>
                              <li><strong>Max. Karyawan</strong></li>
                          </ul>
                        </td>
                        <td>
                          <ul style="list-style-type: none;padding-left:0px">
                            <li>500</li>
                            <li>100</li>
                            <li>5</li>
                            <li>200</li>
                            <li>100</li>
                          </ul>
                        </td>
                        <td class="center">
                         <ul style="list-style-type: none;padding-left:0px">
                            <li>2 Agustus 2018</li>
                            <li>2 Agustus 2018</li>
                            <li>2 Agustus 2018</li>
                            <li>2 Agustus 2018</li>
                            <li>2 Agustus 2018</li>
                          </ul>
                        </td>
                        <td class="center">
                          <button type="button" class="btn btn-xs btn-success""><i class="fa fa-refresh"></i> Perpanjang</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="tab-pane fade" id="lisensi">
                <div class="table-primary">
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center" style="width: 7%">No</th>
                        <th>Item</th>
                        <th>Nomor Lisensi</th>
                        <th>Masa Berlaku</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- TODO : action dropdown, check box -->
                      <tr class="odd gradeX">
                        <td class="text-center">1</td>
                        <td>QWE123</td>
                        <td class="center">
                          FW12-FQEW-12E1-EGQQ
                        </td>
                        <td>
                         <div class="btn btn-success btn-xs">running</div>
                        </td>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="tab-pane fade" id="order">
                <div class="table-primary">
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center" style="width: 7%">No</th>
                        <th>No. Order</th>
                        <th>Tgl. Order</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th style="width: 12%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- TODO : action dropdown, check box -->
                      <tr class="odd gradeX">
                        <td class="text-center">1</td>
                        <td>QWE123</td>
                        <td class="center">
                          25 Juli 2018
                        </td>
                        <td>
                         0
                        </td>
                        <td class="center">
                         Lunas
                        </td>
                        <td class="center">
                          <button type="button" class="btn btn-xs btn-success""><i class="fa fa-eye"></i> Detail</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="tab-pane fade" id="deposit">
                <div class="table-primary">
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center" style="width: 7%">No</th>
                        <th>No. Order</th>
                        <th>Tgl. Order</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th style="width: 12%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- TODO : action dropdown, check box -->
                      <tr class="odd gradeX">
                        <td class="text-center">1</td>
                        <td>QWE123</td>
                        <td class="center">
                          25 Juli 2018
                        </td>
                        <td>
                         0
                        </td>
                        <td class="center">
                         Lunas
                        </td>
                        <td class="center">
                          <button type="button" class="btn btn-xs btn-success""><i class="fa fa-eye"></i> Detail</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
@endsection

@section('modal')

@endsection

@section('custom_css')
<style>
  .select2-dropdown{
  z-index: 9999999999999;
}
  .branch-list h4{
    margin: 10px;
  }
  .branch-list img{
    height: 200px;
  }
</style>
@endsection

@section('custom_js')
  <script>
    // -------------------------------------------------------------------------
    // Initialize Select2

    $(function() {
      $('.select2-example').select2({
        placeholder: 'Select value',
        
      });
    });

    // -------------------------------------------------------------------------
    // Initialize DataTables
    $(function() {
      $('#datatables').dataTable();
      $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
    });

    // -------------------------------------------------------------------------
    // Modal Initialize
    var vmodal = $("#modal");
    var title = vmodal.find(".modal-title");
    var form = $('#form');
    var aksi = form.find('[name="aksi"]');
    var verror = $(".form-group").find("#error");

    // -------------------------------------------------------------------------
    // Modal Open
    function openmodal(vaksi,id=null){

      form[0].reset();
      verror.html('');
      aksi.val(vaksi);
      // mengambil data dri database jika vaksi = edit
      if (vaksi=='edit') {
        title.html('Edit Cabang');
        // $.ajax({

        // });
        form.find('[name="role"]').val("General");
      }else {
        title.html('Tambah Cabang');
      }      
      vmodal.modal('show');
    }

    // -------------------------------------------------------------------------
    // Save Modal
    function save(){
      $.ajax({
        url : "{{ base_url('mgmt/devisi/save') }}",
        type : "POST",
        dataType: "JSON",
        data: form.serialize(),
        beforeSend: function(){
                        // ini operasi apa yang akan dijalankan sebelum request ajax dikirim
                        // biasanya untuk logging jika masih dalam development aplikasi
                        console.log("Request ke method ajax_get_pesan PHP...");

                    },
        success : function(data){
            // mengenai statusnya apabila berhasil atau gagal
            if(data.status){
                // bila request berhasil, tampilkan pesan di HTML
                // data_php disini sesuai dengan index key di array yang ada di method PHP
                // hapus semua error
                verror.html('');
               // terdapat validasi error atau tidak
               if(data.error){
                // jika terdapat error, tampilkan error
                  $.each(data.error, function(key, value){
                      $('[name="'+key+'"]').parents(".form-group").find("#error").html('<label class="col-md-4"></label><p class="col-md-8 text-danger">'+value+'</p>');
                  });
               }else{
                // jika tidak tutup modal
                vmodal.modal('hide');
               }
            }else{
                // apabila gagal didapatkan maka tampilkan pesan ke user agar dapat diketahui statusnya
                // tanpa perlu menunggu lama padahal proses sudah selesai.
                htmlGET.html('Gagal Mendapatkan Data. Silakan Coba Lagi.');
            }
        }
      });
    }  
  </script>
@endsection
