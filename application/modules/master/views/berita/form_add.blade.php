<div class="row">
    <div class="col-lg-8">
        <p><strong>Petunjuk :</strong></p>
        <span>
            Silahkan tambahkan berita yang anda inginkan.
        </span>
    </div>
    <div class="col-lg-12">
    <br>    
        <form class="form-horizontal module-form" role="create">
        <div class="row">
            <div class="col-lg-6">
                 <div class="form-group ">
                <label>Judul Konten</label>
                    <input type="text" class="form-control" name="judul" placeholder="Masukkan judul" required>
            </div>
             <div class="form-group ">
                <label>Kategori Konten</label>
                    <select name="kategori" class="form-control" id="kategori">
                            <option value="">--pilih--</option>
                            <option value="">kategori</option>
                            <option value="">kategori</option>
                    </select>
                    <span>Klasifikasikan konten berdasarkan divisi yang dipilih</span>

            </div>
            </div>

    
            <div class="col-lg-6">
                 <div class="form-group ">
                <label>Status Tampil</label>
                    <select name="status" class="form-control" id="status">
                            <option value="">--pilih--</option>
                            <option value="">DRAFT</option>
                            <option value="">status</option>
                    </select>
            </div>
             <div class="form-group ">
                <label>Bahasa Artikel</label>
                    <select name="bahasa" class="form-control" id="bahasa">
                        <option value="">--pilih--</option>
                        <option value="">INDONESIA</option>
                        <option value="">ENGLISH</option>
                    </select>
                    <span>Pilih salah satu bahasa yang anda gunakanan</span>
            </div>
            </div>

        </div>
        <hr>    
        <div class="row">   
        <div class="col-md-4">
            <label for="">Gambar Cover 1</label>
            <input type="file" name="cover1" class="form-control">
        </div>
        <div class="col-md-4">
            <label for="">Gambar Cover 2</label>
            <input type="file" name="cover2" class="form-control">
        </div>
        <div class="col-md-4">
            <label for="">Gambar Cover 3</label>
            <input type="file" name="cover3" class="form-control"></div>
        </div>
            
        <hr>    
        <textarea name="ckeditor" id="ckeditor" class="ckeditor"></textarea>
        <br>       
          
        
            <div class="form-group m-b-0 text-right">
                <button type="button" class="btn btn-success waves-effect waves-light action-submit">Simpan</button>
            </div>
        </form>
    </div>
</div>
<script src="{{ config('bootstrap', 'assets_back') }}js/sparkline.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/app-style-switcher.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/custom.min.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}ckeditor/ckeditor.js"></script>
<script src="{{ config('bootstrap', 'assets_back') }}js/sample.js"></script>