<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Cabang extends Background_Controller {

	protected $module_url = null;

	/** Constructor */
	function __construct(){
		parent::__construct();
		
		// URL module + controller
		$this->module_url = base_url($this->router->fetch_module().'/'.$this->router->fetch_class());
	}

	// TODO: JAVDOC
	public function index(){
		$data['url']	= $this->module_url;
		$data['title'] 	= "Cabang Perusahaan";
		$this->slice->view('cabang.index', $data);
	}

	public function create(){
		if($this->_ajax_request_validator(self::HTTP_TYPE_POST)){
			$output = ['status' => false];

			// cek validasi form
			$validasi = validation([
				['perusahaan', 'Perusahaan', 'required|trim'],
				['kode', 'Kode Cabang', 'required|trim|max_length[40]'],
				['nama', 'Nama Cabang', 'required|trim'],
				['alamat', 'Email', 'required|trim'],
				['telpon', 'No. Telpon', 'trim|numeric|max_length[20]'],
				['fax', 'No. Fax', 'trim|numeric|max_length[20]'],
				['negara', 'Negara', 'trim|required'],
				['kodepos', 'Kodepos', 'trim|numeric|max_length[6]'],
				['npwp', 'NPWP', 'trim|max_length[40]'],
				['rekening', 'No. Rekening', 'trim|max_length[60]']
			]);

			if($validasi){
				// bila is_hq sudah ada untuk perusahaan ini maka set jadi disable
				$is_hq = (post('is_hq')) ? 1 : 0;
				if($cabang = $this->M_cabang->get(null, ['sys_companies_id' => encrypt_decrypt('decrypt', post('perusahaan')),'is_hq' => 1])){
					$is_hq = 0;
				}

				$data = [
					'nama'				=> post('nama'),
					'kode'				=> strtoupper(post('kode')),
					'alamat'			=> post('alamat'),
					'telpon'			=> post('telpon'),
					'negara'			=> post('negara'),
					'kodepos'			=> post('kodepos'),
					'fax'				=> post('fax'),
					'npwp'				=> post('npwp'),
					'rekening'			=> post('rekening'),
					'is_hq'				=> $is_hq,
					'sys_companies_id'	=> encrypt_decrypt('decrypt', post('perusahaan'))
				];

				// insert data
				$ops = $this->M_cabang->insert(null, $data);
				if($ops){
					$output = ['status' => true, 'msg' => "Data Berhasil Disimpan."];
				}
			}else{
				$output['msg'] = implode(" ", validation_errors_array());
			}

			echo json_encode($output);
		}else{
			// redirect ke main page
			redirect($this->module_url);
		}
	}

	public function edit($id = null){
		$id_raw = encrypt_decrypt('decrypt', $id);

		if($this->_ajax_request_validator(self::HTTP_TYPE_POST) && $id_raw){
			$output = ['status' => false];

			// cek validasi form
			$validasi = validation([
				['perusahaan', 'Perusahaan', 'required|trim'],
				['kode', 'Kode Cabang', 'required|trim|max_length[40]'],
				['nama', 'Nama Cabang', 'required|trim'],
				['alamat', 'Email', 'required|trim'],
				['telpon', 'No. Telpon', 'trim|numeric|max_length[20]'],
				['fax', 'No. Fax', 'trim|numeric|max_length[20]'],
				['negara', 'Negara', 'trim|required'],
				['kodepos', 'Kodepos', 'trim|numeric|max_length[6]'],
				['npwp', 'NPWP', 'trim|max_length[40]'],
				['rekening', 'No. Rekening', 'trim|max_length[60]']
			]);

			if($validasi){
				// bila is_hq sudah ada untuk perusahaan ini maka set jadi disable
				$is_hq = (post('is_hq')) ? 1 : 0;
				if($cabang = $this->M_cabang->get(null, ['sys_companies_id' => encrypt_decrypt('decrypt', post('perusahaan')),'is_hq' => 1])){
					$is_hq = 0;
				}

				$data = [
					'nama'				=> post('nama'),
					'kode'				=> strtoupper(post('kode')),
					'alamat'			=> post('alamat'),
					'telpon'			=> post('telpon'),
					'negara'			=> post('negara'),
					'kodepos'			=> post('kodepos'),
					'fax'				=> post('fax'),
					'npwp'				=> post('npwp'),
					'rekening'			=> post('rekening'),
					'is_hq'				=> $is_hq,
					'sys_companies_id'	=> encrypt_decrypt('decrypt', post('perusahaan'))
				];

				// insert data
				$ops = $this->M_cabang->update(null, ['id' => $id_raw], $data);
				if($ops){
					$output = ['status' => true, 'msg' => "Data Berhasil Disimpan."];
				}else{
					$output['msg'] = "Data Gagal Disimpan.";
				}
			}else{
				$output['msg'] = implode(" ", validation_errors_array());
			}

			echo json_encode($output);
		}else{
			// redirect ke main page
			redirect($this->module_url);
		}
	}

	public function delete(){
		$id_raw = encrypt_decrypt('decrypt', $this->input->input_stream('id'));

		if($this->_ajax_request_validator(self::HTTP_TYPE_DELETE) && $id_raw){
			$output = ['status' => false];

			$ops 	= $this->M_cabang->delete(null, ['id' => $id_raw]);
			if($ops){
				$output = ['status' => true, 'msg' => "Data Berhasil Dihapus."];
			}else{
				$output['msg'] = "Data Gagal Dihapus.";
			}

			echo json_encode($output);
		}else{
			// redirect ke main page
			redirect($this->module_url);
		}
	}

	function ajax_get_page(){
		$output = ['status' => false];
		$output = ['status' => true, 'title' => 'Management Data', 'table' => $this->_data_table(), 'filter' => $this->_data_filter()];

		echo json_encode($output);
	}

	function ajax_get_form(){
		$output = ['status' => false];

		switch(get('form_type')){
			case "add":
				if($this->slice->exists('cabang.form_add')){
					$opt_comp 	= [];

					// data company
					$perusahaan = $this->M_perusahaan->get();
					foreach($perusahaan as $row){
						$opt_comp += [encrypt_decrypt('encrypt', $row->id) => $row->nama];
					}

					$data['url'] 		= $this->module_url;
					$data['perusahaan']	= form_dropdown('perusahaan', $opt_comp, null, 'class="form-control selectpicker" data-live-search="true"');
					$output['status'] 	= true;
					$output['title'] 	= 'Tambah Data';
					$output['filter'] 	= null;
					$output['table'] 	= $this->slice->view('cabang.form_add', $data, true);
				}
				break;
			case "edit":
				if($this->slice->exists('cabang.form_edit')){
					// dapatkan datanya sesuai ID
					if($data_db = $this->M_cabang->get(null, ['id' => encrypt_decrypt('decrypt', get('id'))])){
						$opt_comp 	= [];

						// data company
						$perusahaan = $this->M_perusahaan->get();
						foreach($perusahaan as $row){
							$opt_comp += [encrypt_decrypt('encrypt', $row->id) => $row->nama];
						}

						$data['url'] 		= $this->module_url;
						$data['data_db']	= $data_db;
						$data['perusahaan']	= form_dropdown('perusahaan', $opt_comp, encrypt_decrypt('encrypt', $data_db[0]->sys_companies_id), 'class="form-control selectpicker" data-live-search="true"');
						$output['status'] 	= true;
						$output['title'] 	= 'Edit Data';
						$output['filter'] 	= null;
						$output['table'] 	= $this->slice->view('cabang.form_edit', $data, true);
					}
				}
				break;
			case "detail":
				if($this->slice->exists('cabang.form_detail')){
					// dapatkan datanya sesuai ID
					if($data_db = $this->M_cabang->get(null, ['id' => encrypt_decrypt('decrypt', get('id'))])){
						$opt_comp 	= [];

						// data company
						$nama_perusahaan 	= 'N/A';
						$perusahaan 		= $this->M_perusahaan->get(null, ['id' => $data_db[0]->sys_companies_id]);
						if($perusahaan){
							$nama_perusahaan = $perusahaan[0]->nama;
						}

						$data['url'] 		= $this->module_url;
						$data['data_db']	= $data_db;
						$data['perusahaan']	= $nama_perusahaan;
						$output['status'] 	= true;
						$output['title'] 	= 'Detail Data';
						$output['filter'] 	= null;
						$output['table'] 	= $this->slice->view('cabang.form_detail', $data, true);
					}
				}
				break;
		}

		echo json_encode($output);
	}

	function _data_filter(){
		$opt_comp 	= [];

		// data company
		$perusahaan = $this->M_perusahaan->get();
		foreach($perusahaan as $row){
			$opt_comp += [encrypt_decrypt('encrypt', $row->id) => $row->nama];
		}

		$form = [
			'Perusahaan' => form_dropdown('perusahaan', $opt_comp, get('perusahaan'), 'class="form-control selectpicker" data-live-search="true"')
		];

		return $this->_filter_initialization($this->module_url, $form);
	}

	function _data_table(){
		// inisialisasi tabel
		$this->_table_initialization();

		// header table
		$this->table->set_heading(
            ['data' => 'No', 'class' => 'text-center', 'style' => 'width:8%;'],
			['data' => 'Nama Cabang'],         
            ['data' => 'Dibuat Tanggal', 'class' => 'text-center'],
            ['data' => 'Cabang Pusat?', 'class' => 'text-center'],
            ['data' => 'Action', 'class' => 'text-center', 'style' => 'width:14%;']
		);

		// for ajax GET
		$param = [];
		if(get('GLBQS')){
			parse_str(str_replace('?', null, get('GLBQS')), $get_array);
			$_GET += $get_array;
		}

		if(get('perusahaan')){
			$param += ['sys_companies_id' => encrypt_decrypt('decrypt', get('perusahaan'))];
		}
		
		// dapatkan data
		$db_data = $this->M_cabang->get(null, $param);
		foreach($db_data as $key => $row){
			// created_at
			$tggl_buat 	= Carbon::parse($row->created_at)->format('d M Y');

			// is_hq
			$cab_pusat 	= ($row->is_hq == 1) ? '<strong class="label label-primary">HQ</strong>' : null;

			// nama perusahaan
			$nama_perusahaan 	= 'N/A';
			$perusahaan			= $this->M_perusahaan->get(null, ['id' => $row->sys_companies_id]);
			if($perusahaan){
				$nama_perusahaan = $perusahaan[0]->nama;
			}
			// tombol action
			$action = [
				anchor($this->module_url.'#', '<i class="fa fa-eye fa-fw text-dark"></i>', 'class="action-detail" data-id="'.encrypt_decrypt('encrypt', $row->id).'"'),
				anchor($this->module_url.'#', '<i class="fa fa-edit fa-fw text-dark"></i>', 'class="action-edit" data-id="'.encrypt_decrypt('encrypt', $row->id).'"'),
				anchor($this->module_url.'#', '<i class="fa fa-trash fa-fw text-danger"></i>', 'class="action-delete" data-id="'.encrypt_decrypt('encrypt', $row->id).'"')
			];

			$this->table->add_row(
                ['data' => ++$key, 'class' => 'text-center'],
				['data' => "<h5 class='mb-0'>{$row->nama}</h5><span>{$nama_perusahaan}</span>"],
                ['data' => $tggl_buat, 'class' => 'text-center'],
                ['data' => $cab_pusat, 'class' => 'text-center'],
                ['data' => implode('&nbsp;', $action), 'class' => 'text-center']
            );
		}

		return $this->table->generate();
	}
}
