<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Berita extends Background_Controller {

	protected $module_url = null;

	/** Constructor */
	function __construct(){
		parent::__construct();
		
		// URL module + controller
		$this->module_url = base_url($this->router->fetch_module().'/'.$this->router->fetch_class());
	}

	// TODO: JAVDOC
	public function index(){
		$data['url']	= $this->module_url;
		$data['title'] 	= "Berita";
		$this->slice->view('berita.index', $data);
	}

	public function create(){
		if($this->_ajax_request_validator(self::HTTP_TYPE_POST)){
			$output = ['status' => false];

			// cek validasi form
			$validasi = validation([
				['nama', 'Nama Perusahaan', 'required|trim'],
				['email', 'Email', 'required|valid_email|trim'],
				['website', 'Website', 'required|trim|valid_url']
			]);

			if($validasi){
				$data = [
					'nama'				=> post('nama'),
					'email'				=> post('email'),
					'website'			=> post('website')
				];

				// insert data
				$ops = $this->M_perusahaan->insert(null, $data);
				if($ops){
					$output = ['status' => true, 'msg' => "Data Berhasil Disimpan."];
				}
			}else{
				$output['msg'] = implode(" ", validation_errors_array());
			}

			echo json_encode($output);
		}else{
			// redirect ke main page
			redirect($this->module_url);
		}
	}

	public function edit($id = null){
		$id_raw = encrypt_decrypt('decrypt', $id);

		if($this->_ajax_request_validator(self::HTTP_TYPE_POST) && $id_raw){
			$output = ['status' => false];

			// cek validasi form
			$validasi = validation([
				['nama', 'Nama Perusahaan', 'required|trim'],
				['email', 'Email', 'required|valid_email|trim'],
				['website', 'Website', 'trim|valid_url']
			]);

			if($validasi){
				$data = [
					'nama'				=> post('nama'),
					'email'				=> post('email'),
					'website'			=> post('website')
				];

				// insert data
				$ops = $this->M_perusahaan->update(null, ['id' => $id_raw], $data);
				if($ops){
					$output = ['status' => true, 'msg' => "Data Berhasil Disimpan."];
				}else{
					$output['msg'] = "Data Gagal Disimpan.";
				}
			}else{
				$output['msg'] = implode(" ", validation_errors_array());
			}

			echo json_encode($output);
		}else{
			// redirect ke main page
			redirect($this->module_url);
		}
	}

	public function delete(){
		$id_raw = encrypt_decrypt('decrypt', $this->input->input_stream('id'));

		if($this->_ajax_request_validator(self::HTTP_TYPE_DELETE) && $id_raw){
			$output = ['status' => false];

			$ops 	= $this->M_perusahaan->delete(null, ['id' => $id_raw]);
			if($ops){
				$output = ['status' => true, 'msg' => "Data Berhasil Dihapus."];
			}else{
				$output['msg'] = "Data Gagal Dihapus.";
			}

			echo json_encode($output);
		}else{
			// redirect ke main page
			redirect($this->module_url);
		}
	}

	function ajax_get_page(){
		$output = ['status' => false];
		$output = ['status' => true, 'title' => 'Management Data', 'table' => $this->_data_table(), 'filter' => null];

		echo json_encode($output);
	}



		// memanggil form tambah
	function ajax_get_form(){
		$output = ['status' => false];

		switch(get('form_type')){
			case "add":
				if($this->slice->exists('berita.form_add')){
					$data['url'] 		= $this->module_url;
					$output['status'] 	= true;
					$output['title'] 	= 'Tambah Data';
					$output['filter'] 	= null;
					$output['table'] 	= $this->slice->view('berita.form_add', $data, true);
				}
				break;
			case "edit":
				if($this->slice->exists('berita.form_edit')){
					// dapatkan datanya sesuai ID
					if($data_db = $this->M_perusahaan->get(null, ['id' => encrypt_decrypt('decrypt', get('id'))])){
						$data['url'] 		= $this->module_url;
						$data['data_db']	= $data_db;
						$output['status'] 	= true;
						$output['title'] 	= 'Edit Data';
						$output['filter'] 	= null;
						$output['table'] 	= $this->slice->view('berita.form_edit', $data, true);
					}
				}
				break;
		}

		echo json_encode($output);
	}

	function _data_table(){
		// inisialisasi tabel
		$this->_table_initialization();

		// header table
		$this->table->set_heading(
            ['data' => 'No', 'class' => 'text-center', 'style' => 'width:8%;'],
			['data' => 'Judul Berita','class'  => 'text-center', 'style' => 'width:50%;'],         
			['data' => 'kategori Berita','class'  => 'text-center', 'style' => 'width:12%;'],         
            ['data' => 'Status', 'class' => 'text-center'],
            ['data' => 'Action', 'class' => 'text-center', 'style' => 'width:14%;']
		);
		
		// dapatkan data
		$db_data = $this->M_perusahaan->get();
		foreach($db_data as $key => $row){
			// created_at
			$tggl_buat 	= Carbon::parse($row->created_at)->format('d M Y');

			// jumlah cabang
			$cabang 	= $this->M_cabang->get_count(null, ['sys_companies_id' => $row->id]);

			// tombol action
			$action = [
				anchor($this->module_url.'#', '<i class="fa fa-edit fa-fw text-dark"></i>', 'class="action-edit" data-id="'.encrypt_decrypt('encrypt', $row->id).'"'),
				anchor($this->module_url.'#', '<i class="fa fa-trash fa-fw text-danger"></i>', 'class="action-delete" data-id="'.encrypt_decrypt('encrypt', $row->id).'"')
			];

			$this->table->add_row(
                ['data' => ++$key, 'class' => 'text-center'],
				['data' => "<h5 class='mb-0'>Judul berita</h5><span>"."humas"." &middot; "."<i class='fa fa-list'></i> Bahasa Indonesia"." &middot; "." <i class='fa fa-calendar'></i> 08/08/2018"."</span>"],
				['data' => "Humas",'class' => 'text-center'],
                ['data' => '<a href=publish>PUBLISHED</a>', 'class' => 'text-center'],
                ['data' => implode('&nbsp;', $action), 'class' => 'text-center']
            );
		}

		return $this->table->generate();
	}
}
