<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends Background_Controller {

	protected $module_url = null; 

	/** Constructor */
	function __construct(){
		parent::__construct();

		// URL module + controller 
    	$this->module_url = base_url($this->router->fetch_module().'/'.$this->router->fetch_class()); 
	}
	// TODO: JAVDOC
	public function index(){

		$data['url'] = $this->module_url;
		$data['title'] = "Karyawan";
		$this->slice->view('karyawan.index', $data);
	}

	// TODO: ubah view mnggunakan library table
	function dataview(){
		if(! get()){
			$output = ["status" => false];
		}else{
			// membuat table
			$filter = '<form class="form-horizontal m-b-4">
                <h4 class="text-bold m-t-1">Filter Data</h4> 
                <p>Gunakan form di bawah ini untuk mendapatkan data yang lebih sepesifik.</p>
                <div class="form-group">
                    <label class="col-lg-3 text-center">Kode</label>
                    <div class="col-lg-9">
                        <select class="form-control selectpicker">
                            <option></option>
                            <option>Test</option>
                            <option>Kos</option>
                        </select>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-lg-12"> 
                        <div class="btn-group pull-xs-right"> 
                            <button type="submit" class="btn btn-info">Cari Data</button> 
                        </div> 
                    </div> 
                </div>
            </form>
            <hr>';
			$json = '';
			$json .= '<div class="table-primary"><table class="table table-striped table-bordered" id="datatables">';
			$json .= '	<thead>		
						  <tr>
						    <th class="text-center" style="width: 7%">No</th>
						    <th>Kode</th>
						    <th>Karyawan</th>
						    <th>Tggl Lahir</th>
						    <th>Status</th>
						    <th style="width: 12%">Action</th>
						  </tr>
						</thead>';
			$json .= '	<tbody>';
			// jika terdapat data pada database
			if($data = $this->M_karyawan->get(null)){
				$no = 1;
				foreach ($data as $data) {
					$json .= '
							<tr class="odd gradeX">
								<td class="text-center valign-middle" >'.$no++.'</td>
								<td class="text-center valign-middle">'.$data->kode.'</td>
								<td class="center"><b>'.$data->nama.'</b><br>Email : @gmail.com<br>No. Hp / Telp : '.$data->hp.' / '.$data->telpon.'</td>
								<td class="text-center valign-middle">'.$data->tggl_lahir.'</td>
								<td class="text-center valign-middle"><button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button></td>
								<td class="text-center valign-middle">
									<a href="#" class="update-action font-size-14" data-id="'.encrypt_decrypt('encrypt',$data->id).'"><i class="fa fa-eye text-primary"></i></a>&nbsp;&nbsp
									<a href="#" class="update-action font-size-14" data-id="'.encrypt_decrypt('encrypt',$data->id).'"><i class="fa fa-pencil-square-o text-warning"></i></a>&nbsp
									<a href="#" class="delete-action font-size-14" data-id="'.encrypt_decrypt('encrypt',$data->id).'"><i class="fa fa-trash text-danger"></i></a>
								</td>
							</tr>';
				}
			}else{
				// jika tidak ada pada database

			}
			$json .= '	</tbody>';
			$json .= '</table></div>';
			$output     = ["status" => true, "table" => $json, "title" => "Management Data", "filter" => $filter];
			//penggunaan datatable berdasarkan variabel table jika bernilai true
		}
		echo json_encode($output);	
	}

	// TODO: JAVDOC
	function datapreview(){
		if(! get()){
			$output = ["status" => false];
		}else{
			if (get('aksi')=='tambah') {
				$data['form'] = '';
				$output["status"] = true;
				$output["title"] = 'Tambah Data';
				$output["preview"] = $this->slice->view('karyawan.form', $data, true);
			}elseif (get('aksi')=='edit') {
				$data['form'] = $this->M_role->get(null,["id" => encrypt_decrypt('decrypt',get('id'))]);
				$output["status"] = true;
				$output["title"] = 'Ubah Data';
				$output["preview"] = $this->slice->view('karyawan.form', $data, true);
			}
			
		}
		echo json_encode($output);	
	}

	// TODO: JAVDOC
	function save(){
		if(! post()){
			$output = ["status" => false];
		}else{
			$validasi = validation([
				['code', 'Code', 'required|trim'],
				['nama', 'Nama Role', 'required|trim'],
				['alias', 'Nama Alias', 'required|trim'],
				['sys_branches', 'Branch', 'required|trim'],
			]);
			if($validasi){
				// bila inputan valid
				// array data yg akan diinput
				$data = array(
					'kode' => post('code'),
					'nama' => post('nama'),
					'alias' => post('alias'),
					'is_admin' => post('is_admin'),
					'sys_branches_id' => post('sys_branches')
				);
				// jika aksi merupakan tambah
				if (post('aksi')=='tambah') {
					// $this->M_role->insert(null,$data);
				}
				elseif (post('aksi')=='ubah') {
					// jika aksi merupakan ubah
					// $this->M_role->insert(null,['id' => post('id') ],$data);
				}
				$json = false;
			}else{
				// bila tidak, kembalikan ke halaman sebelumnya
				$json = validation_errors_array();
			}

			$output     = ["status" => true, "error" => $json ];
		}
		echo json_encode($output);	
	}
}
