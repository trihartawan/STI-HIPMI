@extends('layouts.adminweb.master')

@section('header_icon',' fa fa-folder-open')

@section('content')
<div class="col-md-4">
  <div class="panel">
    <div class="panel-heading">
      <div class="panel-title">List Menu</div>
    </div>
    <div class="panel-body sortable-example">
      <div class="col-xs-9">
        <ul id="sortable-1">
          <li class="bg-sidebar"><i class="px-nav-icon fa fa-home"></i><span class="px-nav-label">Dashboard</span><ul></ul></li>
          <li class="bg-sidebar">
            <i class="px-nav-icon fa fa-database"></i><span class="px-nav-label">Data Master</span>
            <ul class="nav-list-menu">
              <li class="bg-sidebar"><span class="px-nav-label">Perusahaan</span><ul></ul></li>
              <li class="bg-sidebar"><span class="px-nav-label">Cabang</span><ul></ul>
              <li class="bg-sidebar"><span class="px-nav-label">Devisi</span><ul></ul>
              <li class="bg-sidebar"><span class="px-nav-label">Karyawan</span><ul></ul>
              </li>
            </ul>
          </li>
          <li class="bg-sidebar">
            <i class="px-nav-icon fa fa-cogs"></i><span class="px-nav-label">Management</span>
            <ul class="nav-list-menu">
              <li class="bg-sidebar"><span class="px-nav-label">Role</span><ul></ul></li>
              <li class="bg-sidebar"><span class="px-nav-label">Pengguna</span><ul></ul>
              <li class="bg-sidebar"><span class="px-nav-label">Modul</span><ul></ul>
              <li class="bg-sidebar"><span class="px-nav-label">Perijinan Sistem</span><ul></ul>
              <li class="bg-sidebar"><span class="px-nav-label">Aplikasi</span><ul></ul>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="col-xs-3"></div>
    </div>
  </div> 
</div>
<div class="col-md-8">
  <div class="panel">
    <div class="panel-heading">
      <div class="panel-title">Tambah / Ubah Menu</div>
    </div>
     <div class="panel-body sortable-example">
      <div class="col-xs-12">
            <ul id="sortable-1">
              <li class="bg-default">Item 1<ul></ul></li>
              <li class="bg-primary">
                Item 2
                <ul>
                  <li class="bg-success">Item 3<ul></ul></li>
                  <li class="bg-info">
                    Item 4
                    <ul>
                      <li class="bg-danger">Item 6<ul></ul></li>
                    </ul>
                  </li>
                </ul>
                </li><li class="bg-warning">Item 5<ul></ul></li>

              <li class="bg-sidebar">Item 7<ul></ul></li>
            </ul>
          </div>
     </div>
  </div> 
</div>

@endsection

@section('modal')
@endsection

@section('custom_css')
 <style>
    .bg-sidebar {
      background-color: #2a2d33;
      color: #a4acba;
    }

     .bg-sidebar:hover {
      color: #fff;
      cursor: pointer;
    }

    .bg-sidebar span:after{
      position: absolute;
      right: 22px;
      display: block;
      width: 5px;
      height: 5px;
      margin-top: -12px;
      content: '';
      -webkit-transform: rotate(45deg);
      -ms-transform: rotate(45deg);
      transform: rotate(45deg);
      border-width: 1px 1px 0 0;
      border-style: solid;
    }

    .bg-sidebar.bg-open>.nav-list-menu{
      display: block;
    }

    .bg-sidebar.bg-open span:after{
      transform: rotate(135deg);
    }
    .nav-list-menu{
      display: none;
      overflow: hidden;
      margin: 0;
      padding: 0;
    }

    .sortable-example ul {
      margin: 0;
      padding: 0;
    }

    .sortable-example li {
      list-style: none;
      border-width: 2px;
      padding: 12px 22px;
    }

    .sortable-example li + li {
      margin-top: 10px;
    }

    .sortable-example ul li {
      margin-top: 10px;
    }

    .sortable-example i.fa-unsorted {
      cursor: move;
    }

    .sortable-example tr.placeholder {
      height: 20px;
    }
  </style>
@endsection

@section('custom_js')
  <script>
     // -------------------------------------------------------------------------
    // Initialize jQuery Sortable
    $(function() {
      $('#sortable-1').sortable();
    });
    $(".bg-sidebar").click(function(){
      if($(this).hasClass("bg-open")){
        $(this).removeClass( "bg-open" );
      }else{
        $(this).addClass( "bg-open" );
      }
    });

  </script>
@endsection
