<form>
  <div class="row">
    <div class="col-md-8">
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">Code</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <input class=" form-control" type="text" name="code" @if(isset($form)) value="{{ $form[0]->kode }}" @endif>
        </div>
      </div>
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">Nama Role</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <input class=" form-control" type="text" name="nama" @if(isset($form)) value="{{ $form[0]->nama }}" @endif>
        </div>
      </div>
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">Alias</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <input class=" form-control" type="text" name="alias" @if(isset($form)) value="{{ $form[0]->alias }}" @endif>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-md-3 text-right">is_admin</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <input type="checkbox" name="is_admin" value="check" @if(isset($form) && $form[0]->is_admin == 1) checked @endif>
        </div>
      </div>
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">Branch</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <select class="form-control select2-example" name="sys_branches" style="width: 100%" data-allow-clear="true">
            <option></option>
          </select>
        </div>
      </div>
    </div>
    <div class="col-md-4">
       <p><b>Petunjuk :</b></p>
        <span>Lengkapi form yang diperlukan di sebelah untuk membuat data baru</span>
    </div>
  </div>
</form>