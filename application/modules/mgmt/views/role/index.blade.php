@extends('layouts.adminweb.master')

@section('header_icon',' fa fa-users')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong class="float-left m-t-5">
                    <i class="fa fa-arrow-circle-right fa-fw"></i>
                    <span class="text-dark module-title">Fetching...</span>
                </strong>
                <span class="float-right btn-group">
                    <button type="button" class="btn btn-sm btn-info btn-main-page waves-effect waves-light">
                        <i class="fa fa-arrow-left fa-fw"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-success waves-effect waves-light action-create">
                        <i class="fa fa-plus fa-fw"></i>
                    </button>
                </span>
            </div>
            <div class="card-body">
                <div class="module-main-content">
                    <!-- FILTER DATA-->
                    <div class="col-lg-6 offset-lg-3 module-content" data-module-item="filter"></div>
                    <!-- MAIN DATA -->
                    <div class="col-lg-12 module-content" data-module-item="table"></div>
                    <!-- SPINNER -->
                    <div class="col-lg-12 module-spinner"></div>
                </div>
            </div>
            <div class="card-footer">
                <strong class="float-right">
                    <span class="module-content" data-module-item="status">Ready</span>
                </strong>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
    // load form dan filter
    var module_url      = '{{ $url }}';
    var btn_main_page   = $('.btn-main-page');
    var module_title    = $('.module-title');
    var module_content  = $('.module-content');
    var module_spinner  = $('.module-spinner');
    var module_status   = $('.module-content[data-module-item="status"]');
    var module_table    = $('.module-content[data-module-item="table"]');
    var module_filter   = $('.module-content[data-module-item="filter"]');

    $(document).ready(function(){
        pageMain();
    });

    $(document).on('click', '.btn-main-page', function(){
        pageMain(true);
    });

    $(document).on('click', '.action-create', function(){
        pageCreate();
    });

    $(document).on('click', '.action-edit', function(){
        data_id = $(this).data('id');
        pageEdit(data_id);
    });

    $(document).on('click', '.action-delete', function(){
        var data_id = $(this).data('id');

        showConfirm().then((result) => {
			if(result.value){
                data_id = $(this).data('id');
                url     = module_url + "/delete";
                setting = {
                    status  : module_status,
                    spinner : module_spinner
                };
                showModulePage(url, "DELETE", setting, { id: data_id }).done(function(data){
                    if(data.status === true){
                        showAlert(true, data.msg);
                    }else{
                        showAlert(false, data.msg);
                    }

                    pageMain();
                });
			}
		});
    });

    $(document).on('click', '.action-submit', function(){
        var module_form = $('.module-form');
        var form_role   = module_form.attr('role');

        form_url    = window.location.href;
        form_data   = module_form.serializeArray();
        setting     = { status  : module_status };
        showModulePage(form_url, "POST", setting, form_data).done(function(data){
            if(data.status === true){
                showAlert(true, data.msg);

                if(form_role == "create"){
                    $('.module-form')[0].reset();
                }
            }else{
                showAlert(false, data.msg);
            }
        });
    });

    function pageCreate(){
        url     = module_url + "/ajax_get_form";
        setting = {
            status  : module_status,
            content : module_content,
            spinner : module_spinner
        };
        showModulePage(url, "GET", setting, { form_type: "add" }, module_url + "/create").done(function(data){
            module_title.text(data.title.toUpperCase());
            module_filter.html(data.filter);
            module_table.html(data.table);
            
            btn_main_page.removeClass('d-none');
        });
    }

    function pageEdit(data_id){
        url     = module_url + "/ajax_get_form";
        setting = {
            status  : module_status,
            content : module_content,
            spinner : module_spinner
        };
        showModulePage(url, "GET", setting, { form_type: "edit", id: data_id }, module_url + "/edit/" + data_id).done(function(data){
            module_title.text(data.title.toUpperCase());
            module_filter.html(data.filter);
            module_table.html(data.table);
            
            btn_main_page.removeClass('d-none');
        });
    }

    function pageMain(is_new_page = false){
        if(is_new_page){
            set_url = module_url;
        }else{
            set_url = null;
        }

        url     = module_url + "/ajax_get_page";
        setting = {
            status  : module_status,
            content : module_content,
            spinner : module_spinner
        };
        showModulePage(url, "GET", setting, {GLBQS: window.location.search}, set_url).done(function(data){
            module_title.text(data.title.toUpperCase());
            module_filter.html(data.filter);
            module_table.html(data.table);
            
            btn_main_page.addClass('d-none');
        });
    }
</script>
@endsection
