<div class="row">
    <div class="col-lg-4">
        <p><strong>Petunjuk :</strong></p>
        <span>
            Lengkapilah form yang tersedia untuk dapat menyimpan data.
        </span>
    </div>
    <div class="col-lg-8">
        <form class="form-horizontal module-form" role="create">
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Kode Role</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="kode" maxlength="40" placeholder="Masukkan Data" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Nama Role</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="nama" placeholder="Masukkan Data" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 text-left control-label col-form-label">Alias Role</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="alias" placeholder="Masukkan Data" required>
                </div>
            </div>
            <div class="custom-control custom-checkbox mr-sm-2 m-b-15">
                <input type="checkbox" class="custom-control-input" name="is_admin" id="cbIsAdmin" value="check">
                <label class="custom-control-label" for="cbIsAdmin">Apakah Ini Role Super Admin?</label>
            </div>
            <div class="form-group m-b-0 text-right">
                <button type="button" class="btn btn-success waves-effect waves-light action-submit">Simpan</button>
            </div>
        </form>
    </div>
</div>