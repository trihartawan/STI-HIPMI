@extends('layouts.adminweb.master')

@section('header_icon',' fa fa-user')

@section('content')
<div class="row">
    <div class="col-lg-3 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30"> 
                    <img src="{{ $data_db[0]->gambar }}" class="rounded-circle" width="150" />
                    <h4 class="card-title m-t-10">{{ $data_db[0]->nama }}</h4>
                    <h6 class="card-subtitle">{{ $role }}</h6>
                    @if($data_db[0]->status == 1)
                        <span class="label label-success"><strong>AKTIF</strong></span>
                    @else
                        <span class="label label-danger"><strong>TIDAK AKTIF</strong></span>
                    @endif
                </center>
            </div>
            <div><hr></div>
            <div class="card-body"> 
                <small class="text-muted">Kode</small>
                <h6>{{ $data_db[0]->kode }}</h6> 
                <small class="text-muted">Email</small>
                <h6>{{ $data_db[0]->email }}</h6> 
                <small class="text-muted">No. Telpon</small>
                <h6>{{ $data_db[0]->telpon }}</h6> 
                <small class="text-muted">No. HP</small>
                <h6>{{ $data_db[0]->hp }}</h6> 
                <small class="text-muted">Tanggal Lahir</small>
                <h6>{{ \Carbon\Carbon::parse($data_db[0]->tggl_lahir)->format('d M Y') }}</h6> 
                <small class="text-muted">Alamat</small>
                <h6>{{ $data_db[0]->alamat }}</h6>
            </div>
        </div>
    </div>
    <div class="col-lg-9 col-xlg-9 col-md-7">
        <div class="card">
            <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-profile" aria-selected="false">Profil</a>
                </li>
                @if($is_self)
                <li class="nav-item">
                    <a class="nav-link" id="pills-timeline-tab" data-toggle="pill" href="#current-month" role="tab" aria-controls="pills-timeline" aria-selected="true">Keamanan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="false">Pengaturan</a>
                </li>
                @endif
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="card-body">
                        <p class="text-justify">
                            <h4 class="text-bold">Profil User</h4>
                            {{ $data_db[0]->profil }}
                        </p>
                    </div>
                </div>
                @if($is_self)
                <div class="tab-pane fade" id="current-month" role="tabpanel" aria-labelledby="pills-timeline-tab">
                    <div class="card-body">
                        <p class="text-justify">
                            <h4 class="text-bold">Keamanan Akun</h4>
                            Tabel di bawah ini adalah riwayat penggunaan / login akun Anda. Apabila Anda tidak mengenali data login
                            di bawah ini, Anda dapat menghapus sesi login tersebut menggunakan tombol yang tersedia.
                        </p>
                        <br/>
                        <div id="res-security">
                            {{ $table }}
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                    <div class="card-body">
                        <p class="text-justify">
                            <h4 class="text-bold">Pengaturan Akun</h4>
                            Apabila terdapat perubahan data pada akun Anda, silakan masukkan melalui form di bawah ini.
                        </p>
                        <br/>
                        <form method="POST" action="{{ current_url() }}" class="form-horizontal form-material">
                            @if(flash('MSG_ERROR'))
                            <div class="form-group">
                                <div class="col-lg-12 alert alert-danger">{{ flash('MSG_ERROR') }}</div>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-sm-12 col-sm-offset-1 control-label">Nama Lengkap</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" value="{{ $data_db[0]->nama }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-sm-offset-1 control-label">Email</label>
                                <div class="col-sm-12">
                                    <input type="email" class="form-control" name="email" placeholder="Email" value="{{ $data_db[0]->email }}" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-sm-offset-1 control-label">Deskripsi</label>
                                <div class="col-sm-12">
                                    <textarea name="profil" class="form-control" rows="3" placeholder="Deskripsi Anda...">{{ strip_tags($data_db[0]->profil); }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">No. Telpon</label>
                                <div class="col-md-12">
                                    <input type="text" name="telpon" placeholder="Masukkan Data" class="form-control form-control-line" value="{{ $data_db[0]->telpon }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">No. HP</label>
                                <div class="col-md-12">
                                    <input type="text" name="hp" placeholder="Masukkan Data" class="form-control form-control-line" value="{{ $data_db[0]->hp }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-sm-offset-1 control-label">Alamat</label>
                                <div class="col-sm-12">
                                    <textarea name="alamat" class="form-control" rows="3" placeholder="Masukkan Data">{{ strip_tags($data_db[0]->alamat); }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Tanggal Lahir</label>
                                <div class="col-md-12">
                                    <input type="date" name="tggl_lahir" class="form-control form-control-line" value="{{ \Carbon\Carbon::parse($data_db[0]->tggl_lahir)->format('Y-m-d') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 col-sm-offset-1 control-label">Password</label>
                                <div class="col-sm-12">
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-default"><i class="fa fa-refresh"></i> Reset Password</button>                                
                                </div>
                            </div>

                            <div class="form-group text-right pt-2">
                                <div class="col-sm-12">
                                    <div class="btn-group">
                                        <button type="submit" name="submit" value="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div id="modal-default" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="POST" action="{{base_url(uri_string())}}">
            <div class="modal-body">
                <p>Untuk mengatur ulang password Anda, silakan masukkan password Anda saat ini, dan password baru Anda</p>
                <div class="form-group">
                    <label class="col-md-12">Password Saat Ini</label>
                    <div class="col-md-12">
                        <input type="password" name="password_current" class="form-control form-control-line" required>
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    <label class="col-md-12">Password Baru</label>
                    <div class="col-md-12">
                        <input type="password" name="password" class="form-control form-control-line" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Ulangi Password Baru</label>
                    <div class="col-md-12">
                        <input type="password" name="password_confirmation" class="form-control form-control-line" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit_password" value="submit_password" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
    $(document).on("click", ".terminate-session", function(){
        var id = $(this).data('id');
        $.ajax({
            url     : "{{ base_url('/mgmt/profile/ajax_terminate_session') }}",
            data    : { token : id },
            type    : "delete",
            dataType: "json",
            success: function(data){
                if(data.status){
                    $('#res-security').html(data.html);
                    $('.datatable').DataTable();
                }
            }
        });
    });
</script>
@endsection
