@extends('layouts.adminweb.master')

@section('header_icon',' fa fa-folder-open')

@section('content')
<div class="panel">
  <div class="panel-heading">
    <div class="text-right">
      <button type="button" class="btn btn-primary" onclick="openmodal(vaksi='tambah')"><i class="fa fa-plus"></i> Tambah Data</button>
    </div>
  </div>
  <div class="panel-body">
    <div class="table-primary col-md-9">
      <table class="table table-striped table-bordered" id="datatables">
        <thead>
          <tr>
            <th class="text-center" style="width: 10%">No</th>
            <th>ID Controller</th>
            <th>ID Method / Access</th>
          </tr>
        </thead>
        <tbody>
          <!-- TODO : action dropdown, check box -->
          <tr>
            <td style="display: none">0</td>
            <td class="text-center" colspan="3" style="background-color: red; color: white"><b>Class Division</b></td>
            <td style="display: none"></td>
          </tr>
          <tr>
            <td class="text-center">1</td>
            <td class="text-center">Division</td>
            <td class="text-center">index</td>
          </tr>
          <tr class="odd gradeX">
            <td class="text-center">2</td>
            <td class="text-center">Division</td>
            <td class="text-center">create</td>
          </tr>
          <tr class="odd gradeX">
            <td class="text-center">3</td>
            <td class="text-center">Division</td>
            <td class="text-center">edit</td>
          </tr>
          <tr class="odd gradeX">
            <td class="text-center">4</td>
            <td class="text-center">Division</td>
            <td class="text-center">delete</td>
          </tr>
          <tr>
            <td style="display: none">4</td>
            <td class="text-center" colspan="3" style="background-color: red; color: white"><b>Class Login</b></td>
            <td style="display: none"></td>
          </tr>
          <tr>
            <td class="text-center">5</td>
            <td class="text-center">Login</td>
            <td class="text-center">index</td>
          </tr>
          <tr class="odd gradeX">
            <td class="text-center">6</td>
            <td class="text-center">Login</td>
            <td class="text-center">forget</td>
          </tr>
          <tr class="odd gradeX">
            <td class="text-center">7</td>
            <td class="text-center">Login</td>
            <td class="text-center">logout</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-3">
      <b>Nama Module</b><br>
      <b style="font-size: 20px; color: #269bc3">Management</b><br>
      <div style="padding-bottom: 5px">
        <b>Versi & Tanggal Install Module</b><br>
        <b style="color: #269bc3">v1.0.0|1 minute ago</b><br>
      </div>
      <b>Tipe Module</b><br>
      <b style="color: #269bc3">System</b><br>
    </div>
  </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<form class="col-md-8" id="form">
	        	<div class="form-group row">
              <div id="error" class="col-md-12"></div>
	        		<label class="col-md-3 text-right">Daftar Module</label>
	        		<label class="col-md-1 text-right">:</label>	
	        		<div class="col-md-8">
	        			<input class=" form-control" type="text" name="code">
	        		</div>
	        	</div>
	        	<div class="form-group row">
              <div id="error" class="col-md-12"></div>
	        		<label class="col-md-3 text-right">Upload Module Baru</label>
	        		<label class="col-md-1 text-right">:</label>	
	        		<div class="col-md-8">
	        			<input class=" form-control" type="text" name="alias">
	        		</div>
              <div class="col-md-1"></div>
              <label class="col-md-4" style="color: grey; font-size: 11px">Maksimal: <b>100M</b>, Format: <b>ZIP</b></label>
	        	</div>
	        </form>
	        <div class="col-md-4">
	           <p><b>Petunjuk :</b></p>
	            <span>Lengkapi form yang diperlukan di sebelah untuk membuat data baru</span>
	        </div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="save()">Save</button>
      </div>
    </div>

  </div>
</div>

@endsection
@section('custom_css')
<style></style>
@endsection

@section('custom_js')
  <script>
    // -------------------------------------------------------------------------
    // Initialize DataTables
    $(function() {
      $('#datatables').dataTable();
      $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
    });

    // -------------------------------------------------------------------------
    // Modal Initialize
    var vmodal = $("#modal");
    var title = vmodal.find(".modal-title");
    var form = $('#form');
    var aksi = form.find('[name="aksi"]');
    var verror = $(".form-group").find("#error");

    // -------------------------------------------------------------------------
    // Modal Open
    function openmodal(vaksi,id=null){

      form[0].reset();
      verror.html('');
      aksi.val(vaksi);
      // mengambil data dri database jika vaksi = edit
      if (vaksi=='edit') {
        title.html('Edit Module');
        // $.ajax({

        // });
        form.find('[name="module"]').val("General");
      }else {
        title.html('Tambah System Module');
      }      
      vmodal.modal('show');
    }

    // -------------------------------------------------------------------------
    // Save Modal
    function save(){
      $.ajax({
        url : "{{ base_url('mgmt/module_sys/save') }}",
        type : "POST",
        dataType: "JSON",
        data: form.serialize(),
        beforeSend: function(){
          // ini operasi apa yang akan dijalankan sebelum request ajax dikirim
          // biasanya untuk logging jika masih dalam development aplikasi
          console.log("Request ke method ajax_get_pesan PHP...");

        },
        success : function(data){
            // mengenai statusnya apabila berhasil atau gagal
            if(data.status){
                // bila request berhasil, tampilkan pesan di HTML
                // data_php disini sesuai dengan index key di array yang ada di method PHP
                // hapus semua error
                verror.html('');
               // terdapat validasi error atau tidak
               if(data.error){
                // jika terdapat error, tampilkan error
                  $.each(data.error, function(key, value){
                      $('[name="'+key+'"]').parents(".form-group").find("#error").html('<label class="col-md-4"></label><p class="col-md-8 text-danger">'+value+'</p>');
                  });
               }else{
                // jika tidak tutup modal
                vmodal.modal('hide');
               }
            }else{
                // apabila gagal didapatkan maka tampilkan pesan ke user agar dapat diketahui statusnya
                // tanpa perlu menunggu lama padahal proses sudah selesai.
                htmlGET.html('Gagal Mendapatkan Data. Silakan Coba Lagi.');
            }
        }
      });
    }
  </script>
@endsection
