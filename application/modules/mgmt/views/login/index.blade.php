@extends('layouts.login.master')

@section('content')
<div class="row">
    <div class="col-12">
        @if(flash('UERROR'))
            <div class="alert alert-danger">{{ flash('UERROR') }}</div>
        @endif
        <form method="POST" action="{{ current_url() }}" class="form-horizontal m-t-10" id="loginform">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                    </div>
                    <input type="email" class="form-control form-control-md" placeholder="Email" name="email" required>
                </div>
                @if(isset(flash('ERROR')['email']))
                    <small class="form-text text-danger">{{ flash('ERROR')['email'] }}</small>
                @endif
            <div>
            <div class="form-group mt-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-asterisk"></i></span>
                    </div>
                    <input type="password" class="form-control form-control-md" placeholder="Password" name="password" required>
                </div>
                @if(isset(flash('ERROR')['password']))
                    <small class="form-text text-danger">{{ flash('ERROR')['password'] }}</small>
                @endif
            <div>
            <div class="row pt-2 pb-4">
                <div class="col-lg-12 text-right">
                    <a href="{{ base_url('forgot') }}" class="text-dark">
                        <i class="fa fa-lock fa-fw"></i> Lupa Kata Sandi?
                    </a>
                </div>
            </div>
            <div class="form-group text-center">
                <div class="col-xs-12 p-b-20">
                    <button class="btn btn-block btn-lg btn-success" type="submit">Login</button>
                </div>
            </div>
            <div class="form-group m-b-0 m-t-10">
                <div class="col-sm-12 text-center">
                    &COPY; {{ Carbon\Carbon::now()->year }} - {{ anchor(base_url(), COMPANY_NAME, 'class="text-primary"') }}
                </div>
            </div>
        </form>
    </div>
</div>
@endsection