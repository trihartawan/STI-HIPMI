@extends('layouts.adminweb.master')

@section('content')
<div class="panel">
  <div class="panel-heading">
    <button type="button" class="btn btn-primary back-action"><i class="fa fa-arrow-left"></i> Kembali</button>
  </div>
  <div class="panel-body">
    <div class="col-md-10 col-md-offset-1">
      <div class="row m-t-2">
        <span class="col-md-5 col-sm-12 text-center" style="padding-top:45px;">
            <i class="fa fa-bell" style="color:#33414e; font-size:60pt;"></i><br><br>
            <h4 class="m-t-1"><b>NOTIFIKASI</b></h4>
        </span>
        <div class="col-md-7">
          <strong>PETUNJUK:<br></strong>
          <ul class="font-size-12" style="margin-left:-25px;">
              <li>Di bawah ini merupakan daftar notifikasi Anda.</li>
              <li>Notifikasi yang belum Anda lihat atau anda baca ditandai dengan warna yang berbeda.</li>
              <li>Notifikasi dengan prioritas penting akan ditandai dengan label <label class="label label-info">PENTING</label></li>
              <li>Beberapa notifikasi mungkin akan mengarahkan Anda ke halaman tertentu apabila Anda membuka tautan pada notifikasi terkait.</li>
              <li>Notifikasi yang memiliki data atau keterangan lebih lanjut akan ditampilkan dengan keterangan.</li>
              <li>Untuk melihat notifikasi yang telah berlalu Anda dapat melakukan scrolling ke bawah halaman ini, sistem akan 
              menampilkan notifikasi Anda lainnnya (apabila masih ada).</li>
              
          </ul>
        </div>
      </div>
      <hr>
      <div class="row m-t-1 m-b-4">
        <div class="col-sm-12">
          <button type="button" class="btn btn-primary back-action"><i class="fa fa-check"></i> Mark All as Read</button>
          <hr>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('modal')

@endsection
@section('custom_css')
 <style>
    .profile {
      display: flex;
    }
    .profile-name {
      margin-top: 40px;
      margin-bottom: 0px;
    }
   .profile span {
      padding-top: 4px;
    }

    .page-profile-v1-avatar {
      max-width: 160px;
      border: 4px solid #fff;
    }
  </style>
@endsection

@section('custom_js')
  <script>

  </script>
@endsection
