<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Profile extends Background_Controller {

	/** Constructor */
	function __construct(){
		parent::__construct();
	}

	// TODO: JAVDOC
	public function index($id = null){
		// submit dan non submit
        if(! post('submit') && ! post('submit_password')){
            // bila user id kosong maka tampilkan datanya saja        
            $user_id = (isset($id)) ? encrypt_decrypt('decrypt', $id) : session(BACKGROUND_SESSION)['user_id'];
            $result  = $this->M_login->get(null, [ 'id' => $user_id ]);
            if($result){
                // dapatkan datanya berdasarkan tabel
                $table_lookup = $this->M_login->get('sys_user_tables', ['id' => $result[0]->sys_user_tables_id]);
                if($table_lookup){
                    $detail = $this->M_karyawan->get($table_lookup[0]->table, ['kode' => $result[0]->kode]);
                    if($detail){
                        // field-field yang dibutuhkan
                        $required_field = ['nama', 'alamat', 'telpon', 'hp', 'profil', 'gambar', 'tggl_lahir'];

                        // masukkan semua field yang dibutuhkan ke variable $result
                        foreach($required_field as $row){
                            if($row == "gambar"){
                                // data profil picture
                                if($detail[0]->$row == null){
                                    $result[0]->{$row} = config('bootstrap', 'assets_back')."img/avatars/default.png";
                                }else{
                                    $result[0]->{$row} = $detail[0]->$row;
                                }
                            }else{
                                // data lainnya
                                if(isset($detail[0]->$row)){
                                    $result[0]->{$row} = $detail[0]->$row;
                                }else{
                                    $result[0]->{$row} = 'N/A';
                                }
                            }
                        }
                    }
                }

                $nama_role  = 'N/A';
                $role       = $this->M_role->get(null, ['id' => $result[0]->sys_roles_id]);
                if($role){
                    $nama_role = $role[0]->nama;
                }
				
                // bila ini profile user sendiri
                if($user_id == session(BACKGROUND_SESSION)['user_id']){
                    $title      = "Profil Saya"; 
                    $is_self    = true;
                }else{
                    $title = "Profil {$result[0]->nama}"; 
                    $is_self    = false;
                }

                $data['is_self']    = $is_self;
                $data['role']       = $nama_role;
                $data['title']      = $title;
                $data['table']      = $this->_table_session();
                $data['divisi']     = 'Tidak Ada Divisi';
                $data['data_db']    = $result;

                $this->slice->view('profile.index', $data);
            }else{
                show_error();
            }
        }elseif(post('submit')){
            
            // reject bila bukan diri sendiri
            if(! empty($user_id) && $user_id != session(BACKGROUND_SESSION)['user_id']){
                return false;
            }

            $form_validate = validation([
                ['nama', 'Nama Lengkap', 'trim|required'],
                ['email', 'Email', 'trim|required'],
                ['profil', 'Profil', 'trim|xss_clean'],
                ['telpon', 'No. Telpon', 'trim|numeric'],
                ['hp', 'No. HP', 'trim|numeric'],
                ['tggl_lahir', 'Tanggal Lahir', 'trim'],
                ['alamat', 'Alamat', 'trim|xss_clean']
            ]);

            if($form_validate){
                $data_login = [];
                $data_user  = [];
                $sess = session(BACKGROUND_SESSION);

                if(post('email') != $sess['user_email']){
                    // cek apakah email sdh digunakan
                    $email = $this->M_login->get_count(null, ['email' => post('email')]);
                    if($email > 0){
                        // sudah digunakan
                        flash(['MSG_FAILED' => 'Email Sudah Digunakan.']);
                    }

                    // email ganti, kirim ke email baru untuk konfirmasi
                    $data_login += [ 'email' => post('email') ];
                    $sess['user_email'] = post('email');
                }

                // data baru untuk diupdate
                $data_user += [ 
                    'nama'          => post('nama'),
                    'profil'        => post('profil'),
                    'alamat'        => post('alamat'),
                    'telpon'        => post('telpon'),
                    'hp'            => post('hp'),
                    'tggl_lahir'    => post('tggl_lahir')
                ];

                // data session baru
                $sess['user_fullname'] = post('nama');

                // upate data master
                if($data_login){
                    $i = $this->M_login->update(null, [ 'id' => session(BACKGROUND_SESSION)['user_id'] ], $data_login);
                }

                $i = $this->M_karyawan->update(null, ['kode' => session(BACKGROUND_SESSION)['user_kode'] ], $data_user);
                if($i){
                    // update session
                    session([ BACKGROUND_SESSION => $sess ]);
                    
                    // success message 
                    flash(['MSG_SUCCESS' => 'Data Berhasil Disimpan.']);
                }else{
                    // fail message 
                    flash(['MSG_FAILED' => 'Data Gagal Disimpan, Silakan Ulangi Lagi.']);
                }

                // kembali ke halaman sebelumnya
                redirect($this->agent->referrer());                
            }else{
                flash(['MSG_ERROR' => validation_errors()]);
                redirect($this->agent->referrer());
            }
        }elseif(post('submit_password')){
            
            // reject bila bukan diri sendiri
            if(! empty($user_id) && $user_id != session(BACKGROUND_SESSION)['user_id']){
                return false;
            }

            // validate form
            $form_validate = validation([
                ['password_current', 'Password Saat Ini', 'required'],                
                ['password', 'Password', 'required'],
                ['password_confirmation', 'Konfirmasi Password', 'required|matches[password]'],
            ]);

            if($form_validate){
                if($user = $this->M_login->get(null, [ 'id' => session(BACKGROUND_SESSION)['user_id'] ])){
                    $salt 		= $user[0]->salt;
                    $password 	= sha1(post('password_current').$salt);

                    if($password != $user[0]->password){
                        // fail message 
                        flash(['MSG_FAILED' => 'Password Lama Tidak Sesuai.']);
                    }else{
                        $salt = randstr('alnum', 64);
                        $data = [ 
                            'salt'      => $salt,
                            'password'  => sha1(post('password').$salt, FALSE)
                        ];

                        // $i = $this->M_login->update(null, [ 'id' => session(BACKGROUND_SESSION)['user_id'] ], $data);
                        if($i){
                            flash(['MSG_SUCCESS' => 'Password Berhasil Diganti.']);
                        }else{
                            flash(['MSG_FAILED' => 'Password Gagal Diganti, Silakan Ulangi Lagi.']);                
                        }
                    }
                }else{
                    flash(['MSG_FAILED' => 'User Tidak Ditemukan.']);
                }

                // kembali ke halaman sebelumnya
                redirect($this->agent->referrer());                
            }else{
                flash(['MSG_ERROR' => validation_errors()]);
                redirect($this->agent->referrer());                
            }
        }
	}

	public function ajax_terminate_session(){

        $token  = $this->M_session->get('sys_sessions', [ 'token' => session(BACKGROUND_SESSION)['token'] ]);

        if($this->_ajax_request_validator(self::HTTP_TYPE_DELETE) && $token){
            // predefault ajax output
            $output = [ 'status' => false ];

            // bila token ini milik user sendiri
            if($token[0]->sys_users_id == session(BACKGROUND_SESSION)['user_id']){
                // hapus sessionnya
                $d = $this->M_session->delete('sys_sessions', [ 'token' => $this->input->input_stream('token') ]);
                if($d){
                    $output = [ 'status' => true, 'html' => $this->_table_session() ];
                }
            }

            echo json_encode($output);
		}
	}
	
	public function ajax_get_notification(){
        
        // predefault ajax output
        $output = [ 'status' => false ];

        if($this->request_method_get){
            // offset dari ajax request
            $offset = get('next_number');

            // load jumlah notifikasinya
            $param = [ 'app_users_id' => $this->user_data->user_id ];
            if(get('read') !== null){
                $param += [ 'is_read' => get('read') ];        
            }

            $max_data       = $this->M_notification->get_count(null, [ 'app_users_id' => $this->user_data->user_id ]);
            $data['notif']  = $this->M_notification->get(null, $param, null, ['created_at' => 'desc'], null, $this->max_notif, $offset);

            $data['last_d'] = get('tgl');;
            $view           = $this->slice->view('profile.notifications_partial', $data, TRUE);
            
            $last_date      = Carbon::parse($data['notif'][count($data['notif']) - 1]->created_at)->format('d M Y');

            $output         = [ 
                'status'    => true,
                'html'      => $view,
                'count'     => count($data['notif']),
                'max_num'   => $max_data,
                'last_date' => $last_date
            ]; 
        }

        echo json_encode($output);
    }

    public function ajax_read_notification(){
        if($this->request_method_patch){
            // get patch ID
            $id     = $this->request_data['id'];
            $data   = ['is_read' => 1];
            $this->M_notification->update(null, [ 'id' => $id ], $data);

            echo true;
        }
    }

    function _table_session(){
        // inisialisasi tabel
		$this->_table_initialization();

		// header table
		$this->table->set_heading(
            ['data' => '<i class="fa fa-tv fa-fw ml-1"></i> Platform'],
			['data' => '<i class="fa fa-link fa-fw"></i> IP Address', 'class' => 'text-center', 'style' => 'width:10%;'],         
            ['data' => '<i class="fa fa-clock fa-fw"></i> Time Login', 'class' => 'text-center'],
            ['data' => 'Action', 'class' => 'text-center', 'style' => 'width:14%;']
		);

		$db_data = $this->M_session->get('sys_sessions', [ 'sys_users_id' => session(BACKGROUND_SESSION)['user_id'] ], null, ['created_at' => 'desc']);
		foreach($db_data as $key => $row){
            // tombol action
            $action = [];
            if(session(BACKGROUND_SESSION)['token'] != $row->token){
                $action = [
                    anchor(base_url('/profil#'), '<i class="fa fa-times fa-fw"></i>', 'data-id="'.$row->token.'" class="btn btn-danger btn-sm terminate-session"'),
                ];
            }

            $this->agent->parse($row->user_agent);
            $paltform = ($this->agent->is_mobile()) ? "Mobile" : "Desktop";

			$this->table->add_row(
                ['data' => "<h5 class='mb-0 ml-1'>".$this->agent->platform()."</h5>
                            <span class='ml-1'>{$paltform} &middot; {$this->agent->browser()}</span>"],
                ['data' => $row->ip_address, 'class' => 'text-center'],
                ['data' => Carbon::parse($row->created_at)->diffForHumans(), 'class' => 'text-center'],
                ['data' => implode('&nbsp;', $action), 'class' => 'text-center']
            );
		}

		return $this->table->generate();
    }
	
	function _get_notifications(){
        $output = [
            'unread'    => $this->M_notification->get_count(null, ['app_users_id' => $this->user_data->user_id, 'is_read' => 0]),
            'konten'    => $this->M_notification->get(null, ['app_users_id' => $this->user_data->user_id], null, ['created_at' => 'desc'], null, 20)
        ];

        return $output;
    }
}
