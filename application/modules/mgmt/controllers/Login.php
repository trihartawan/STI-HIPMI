<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Login extends Foreground_Controller {

	/** Constructor */
	function __construct(){
		parent::__construct();
	}

	/**
	 * Halaman utama login
	 * 
	 * @return	CI_Output
	 */
	public function index(){
		if(! post()){
			// bila user sudah login, redirect
			if(session(BACKGROUND_SESSION)){
				redirect(base_url('beranda'));
			}else{
				// tampilkan halaman
				$data['title'] = "Login";
				$this->slice->view('login.index', $data);
			}
		}else{
			// cek validasi form
			$validasi = validation([
				['email', 'Email', 'required|trim|valid_email'],
				['password', 'Password', 'required|trim']
			]);

			if($validasi){
				// bila inputan valid
				if($user = $this->M_login->get(null, ['email' => post('email'), 'status' => 1])){
					// dapatkan salt user dan password user
					$salt 		= $user[0]->salt;
					$password 	= sha1(post('password').$salt);

					// bila password user sama dengan yang ada didatabase
					// maka ijinkan login
					if($password == $user[0]->password){
						// dapatkan tabel user ini
						if($user_tabel = $this->M_user_table->get(null, ['id' => $user[0]->sys_user_tables_id])){
							$table = $user_tabel[0]->table;

							// generate token dan session
							$token 			= random_string('alnum', 80);
							$data_session 	= [
								'token'			=> $token,
								'user_agent'	=> $this->agent->agent_string(),
								'ip_address'	=> $this->input->ip_address(),
								'sys_users_id'	=> $user[0]->id
							];

							// insert session ke database
							$session = $this->M_session->insert(null, $data_session);

							// set session
							$data_session 	= [
								'user_id'		=> $user[0]->id,
								'user_kode'		=> $user[0]->kode,
								'user_email'	=> $user[0]->email,
								'user_table'	=> $table,
								'user_roles_id'	=> $user[0]->sys_roles_id,
								'token'			=> $token
							];

							// set session
							session([BACKGROUND_SESSION => $data_session]);

							// redirect ke beranda
							redirect(base_url('beranda'));
						}else{
							// bila tidak, kembalikan ke halaman sebelumnya
							flash(['UERROR' => "User tidak dapat ditemukan."]);
							redirect($this->back);
						}
					}else{
						// bila tidak, kembalikan ke halaman sebelumny
						flash(['UERROR' => "Password salah."]);
						redirect($this->back);
					}
				}else{
					// bila tidak, kembalikan ke halaman sebelumnya
					flash(['UERROR' => "User tidak dapat ditemukan."]);
					redirect($this->back);
				}
			}else{
				// bila tidak, kembalikan ke halaman sebelumnya
				flash(['ERROR' => validation_errors_array()]);
				redirect($this->back);
			}
		}	
	}

	/**
	 * Halaman untuk reset password user terdaftar
	 * 
	 * @return	CI_Output
	 */
	public function lupa_password(){
		if(! post()){
			// bila user sudah login, redirect
			if(session(BACKGROUND_SESSION)){
				redirect(base_url('beranda'));
			}else{
				// tampilkan halaman
				$data['title'] = "Lupa Password";
				$this->slice->view('login.forgot', $data);
			}
		}else{
			// proses POST
			// cek validasi form
			$validasi = validation([
				['email', 'Email', 'required|trim|valid_email']
			]);

			if($validasi){
				// bila inputan valid
				if($user = $this->M_login->get(null, ['email' => post('email')])){
					// email terdaftar, update password user
					$salt 		= random_string('alnum', 80);
					$password 	= random_string('numeric', 8);
					$data		= [
						'salt'			=> $salt,
						'password'		=> sha1($password.$salt)
					];

					$update = $this->M_login->update(null, ['id' => $user[0]->id], $data);
					if($update){
						// kirim email ke user
						$msg = "Password Anda telah direset menjadi berikut ini:<br/><strong>Password: {$password}</strong><br/>Segera ganti password Anda setelah login.<br/><br/>Terima Kasih";
						send_email("Reset Password", $msg, null, $user[0]->email);

						flash(['MSG_SUCCESS' => "Email sudah dikirim, silakan cek Email Anda."]);
					}
				}else{
					// email tidak terdaftar, tampilkan halaman error
					flash(['UERROR' => "Email tidak ditemukan."]);
				}
			}else{
				// bila tidak, kembalikan ke halaman sebelumnya
				flash(['ERROR' => validation_errors_array()]);
			}

			redirect($this->back);
		}
	}
	
	/**
	 * Logout dan hapus data session user
	 * 
	 * @return	CI_Output
	 */
	public function logout(){
		if(session(BACKGROUND_SESSION)){
			// dapatkan data session user
			$session = session(BACKGROUND_SESSION);

			// hapus data session didatabase
			$delete_session = $this->M_session->delete(null, ['token' => $session['token'], 'sys_users_id' => $session['user_id']]);

			// destroy session 
			$this->session->unset_userdata(BACKGROUND_SESSION);
		}

		// redirect ke halaman login
		redirect(base_url('login'));
	}
}
