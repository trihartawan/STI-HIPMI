<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module_app extends STI_Controller {

	/** Constructor */
	function __construct(){
		parent::__construct();
	}
	// TODO: JAVDOC
	public function index(){
		$data['title'] = "Management Application Module";
		$this->slice->view('module_app.index', $data);
	}
}