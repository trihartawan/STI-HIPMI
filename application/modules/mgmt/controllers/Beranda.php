<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends Background_Controller {

	/** Constructor */
	function __construct(){
		parent::__construct();
	}
	
	// TODO: JAVDOC
	public function index(){
		$data['title'] = "Beranda";
		$this->slice->view('beranda.index', $data);
	}
}
