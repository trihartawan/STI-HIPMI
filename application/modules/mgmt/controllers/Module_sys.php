<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module_sys extends STI_Controller {

	/** Constructor */
	function __construct(){
		parent::__construct();
	}
	// TODO: JAVDOC
	public function index(){
		$data['title'] = "Management System Module";
		$this->slice->view('module_sys.index', $data);
	}
}
