<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends STI_Controller {

	/** Constructor */
	function __construct(){
		parent::__construct();
	}
	// TODO: JAVDOC
	public function index(){
		$data['title'] = "Management Role";
		$this->slice->view('role.index', $data);
	}

	// TODO: JAVDOC
	function save(){
		if(! post()){
			$output = ["status" => false];
		}else{
			$validasi = validation([
				['code', 'Code', 'required|trim'],
				['nama', 'Nama Role', 'required|trim'],
				['alias', 'Nama Alias', 'required|trim'],
			]);
			$json = '';
			if($validasi){
				// bila inputan valid
				
			}else{
				// bila tidak, kembalikan ke halaman sebelumnya
				$json = validation_errors_array();
			}

			$output     = ["status" => true, "error" => $json];
		}
		echo json_encode($output);	
	}
}
