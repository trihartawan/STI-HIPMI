<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends Background_Controller {

	protected $module_url = null;

	/** Constructor */
	function __construct(){
		parent::__construct();

		// URL module + controller
		$this->module_url = base_url($this->router->fetch_module().'/'.$this->router->fetch_class());
	}

	// TODO: JAVDOC
	public function index(){
		$data['url']	= $this->module_url;
		$data['title'] 	= "Role";
		$this->slice->view('role.index', $data);
	}

	public function create(){
		if($this->_ajax_request_validator(self::HTTP_TYPE_POST)){
			$output = ['status' => false];

			// cek validasi form
			$validasi = validation([
				['nama', 'Nama', 'required|trim'],
				['alias', 'Alias', 'required|alpha_dash|trim'],
				['kode', 'Kode', 'required|trim|alpha_numeric|max_length[40]']
			]);

			if($validasi){
				$data = [
					'kode' 				=> strtoupper(post('kode')),
					'nama'				=> post('nama'),
					'alias'				=> post('alias'),
					'is_admin'			=> (post('is_admin')) ? 1 : 0,
					'sys_branches_id'	=> 1
				];

				// insert data
				$ops = $this->M_role->insert(null, $data);
				if($ops){
					$output = ['status' => true, 'msg' => "Data Berhasil Disimpan."];
				}
			}else{
				$output['msg'] = implode(" ", validation_errors_array());
			}

			echo json_encode($output);
		}else{
			// redirect ke main page
			redirect($this->module_url);
		}
	}

	public function edit($id = null){
		$id_raw = encrypt_decrypt('decrypt', $id);

		if($this->_ajax_request_validator(self::HTTP_TYPE_POST) && $id_raw){
			$output = ['status' => false];

			// cek validasi form
			$validasi = validation([
				['nama', 'Nama', 'required|trim'],
				['alias', 'Alias', 'required|alpha_dash|trim'],
				['kode', 'Kode', 'required|trim|alpha_numeric|max_length[40]']
			]);

			if($validasi){
				$data = [
					'kode' 				=> strtoupper(post('kode')),
					'nama'				=> post('nama'),
					'alias'				=> post('alias'),
					'is_admin'			=> (post('is_admin')) ? 1 : 0,
					'sys_branches_id'	=> 1
				];

				// insert data
				$ops = $this->M_role->update(null, ['id' => $id_raw], $data);
				if($ops){
					$output = ['status' => true, 'msg' => "Data Berhasil Disimpan."];
				}else{
					$output['msg'] = "Data Gagal Disimpan.";
				}
			}else{
				$output['msg'] = implode(" ", validation_errors_array());
			}

			echo json_encode($output);
		}else{
			// redirect ke main page
			redirect($this->module_url);
		}
	}

	public function delete(){
		$id_raw = encrypt_decrypt('decrypt', $this->input->input_stream('id'));

		if($this->_ajax_request_validator(self::HTTP_TYPE_DELETE) && $id_raw){
			$output = ['status' => false];

			$ops 	= $this->M_role->delete(null, ['id' => $id_raw]);
			if($ops){
				$output = ['status' => true, 'msg' => "Data Berhasil Dihapus."];
			}else{
				$output['msg'] = "Data Gagal Dihapus.";
			}

			echo json_encode($output);
		}else{
			// redirect ke main page
			redirect($this->module_url);
		}
	}

	function ajax_get_page(){
		$output = ['status' => false];
		$output = ['status' => true, 'title' => 'Management Data', 'table' => $this->_data_table(), 'filter' => $this->_data_filter()];

		echo json_encode($output);
	}

	function ajax_get_form(){
		$output = ['status' => false];

		switch(get('form_type')){
			case "add":
				if($this->slice->exists('role.form_add')){
					$data['url'] 		= $this->module_url;
					$output['status'] 	= true;
					$output['title'] 	= 'Tambah Data';
					$output['filter'] 	= null;
					$output['table'] 	= $this->slice->view('role.form_add', $data, true);
				}
				break;
			case "edit":
				if($this->slice->exists('role.form_edit')){
					// dapatkan datanya sesuai ID
					if($data_db = $this->M_role->get(null, ['id' => encrypt_decrypt('decrypt', get('id'))])){
						$data['url'] 		= $this->module_url;
						$data['data_db']	= $data_db;
						$output['status'] 	= true;
						$output['title'] 	= 'Edit Data';
						$output['filter'] 	= null;
						$output['table'] 	= $this->slice->view('role.form_edit', $data, true);
					}
				}
				break;
		}

		echo json_encode($output);
	}

	function _data_filter(){
		$opt_sts = [
			encrypt_decrypt('encrypt', (int) 0) => 'Role Normal',
			encrypt_decrypt('encrypt', (int) 1) => 'Super Administrator'
		];

		$form 	 = [
			'Status Role' => form_dropdown('status', $opt_sts, get('status'), 'class="form-control selectpicker" data-live-search="true"')
		];

		return $this->_filter_initialization($this->module_url, $form);
	}

	function _data_table(){
		// inisialisasi tabel
		$this->_table_initialization();

		// header table
		$this->table->set_heading(
            ['data' => 'No', 'class' => 'text-center', 'style' => 'width:8%;'],
			['data' => 'Nama Role'],         
            ['data' => 'Status Role', 'class' => 'text-center'],
            ['data' => 'Action', 'class' => 'text-center', 'style' => 'width:14%;']
		);

		// for ajax GET
		$param = [];
		if(get('GLBQS')){
			parse_str(str_replace('?', null, get('GLBQS')), $get_array);
			$_GET += $get_array;
		}

		if(get('status')){
			$param += ['is_admin' => encrypt_decrypt('decrypt', get('status'))];
		}
		
		// dapatkan data
		$db_data = $this->M_role->get(null, $param);
		foreach($db_data as $key => $row){
			// is admin?
			$is_admin = ($row->is_admin == 1) ? "<strong class='text-danger'>Super Admin</strong>" : "Role Normal";

			// tombol action
			$action = [
				anchor($this->module_url.'#', '<i class="fa fa-edit fa-fw text-dark"></i>', 'class="action-edit" data-id="'.encrypt_decrypt('encrypt', $row->id).'"'),
				anchor($this->module_url.'#', '<i class="fa fa-trash fa-fw text-danger"></i>', 'class="action-delete" data-id="'.encrypt_decrypt('encrypt', $row->id).'"')
			];

			$this->table->add_row(
                ['data' => ++$key, 'class' => 'text-center'],
				['data' => "<h5 class='mb-0'>{$row->nama}</h5><span>Kode: {$row->kode} &middot; Alias: {$row->alias}</span>"],
                ['data' => $is_admin, 'class' => 'text-center'],
                ['data' => implode('&nbsp;', $action), 'class' => 'text-center']
            );
		}

		return $this->table->generate();
	}
}
