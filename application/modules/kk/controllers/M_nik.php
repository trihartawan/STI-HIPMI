<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_nik extends Background_Controller {

	/** Constructor */
	function __construct(){
		parent::__construct();

		// URL module + controller 
    	$this->module_url = base_url($this->router->fetch_module().'/'.$this->router->fetch_class()); 
	}
	// TODO: JAVDOC
	public function index(){

		$data['url'] = $this->module_url;
		$data['title'] = "Management NIK";
		$this->slice->view('m_nik.index', $data);
	}

	function ajax_get_page(){  
	  $output = ['status' => false];  
	  $output = ['status' => true, 'title' => 'Management Data', 'table' => $this->_data_table(), 'filter' => $this->_data_filter()];  

	  echo json_encode($output);  
	} 
 
	function ajax_get_form(){  
		$output = ['status' => false];  

		switch(get('form_type')){  
		  case "add":  
		    if($this->slice->exists('m_nik.form')){  
		      $data['url']     = $this->module_url;  
		      $output['status']   = true;  
		      $output['title']   = 'Tambah Data';  
		      $output['filter']   = null;  
		      $output['table']   = $this->slice->view('m_nik.form', $data, true);  
		    }  
		    break;  
		  case "edit":  
		    if($this->slice->exists('role.form')){  
		      // dapatkan datanya sesuai ID  
		      if($data_db = $this->M_role->get(null, ['id' => encrypt_decrypt('decrypt', get('id'))])){  
		        $data['url']     = $this->module_url;  
		        $data['data_db']  = $data_db;  
		        $output['status']   = true;  
		        $output['title']   = 'Edit Data';  
		        $output['filter']   = null;  
		        $output['table']   = $this->slice->view('role.form', $data, true);  
		      }  
		    }  
		    break;
		   case "detail":  
		    if($this->slice->exists('m_nik.detail')){  
			    $data['url']     = $this->module_url;  
		      $output['status']   = true;  
		      $output['title']   = 'Detail Data';  
		      $output['filter']   = null;  
		      $output['table']   = $this->slice->view('m_nik.detail', $data, true);  
		      // dapatkan datanya sesuai ID  
		      // if($data_db = $this->M_role->get(null, ['id' => encrypt_decrypt('decrypt', get('id'))])){  
		          
		      // }  
		    }  
		    break;  
		}  

		echo json_encode($output);  
	}  

		public function create(){ 
	    if($this->_ajax_request_validator(self::HTTP_TYPE_POST)){ 
	      $output = ['status' => false]; 
	 
	      // cek validasi form 
	      $validasi = validation([ 
	        ['nama', 'Nama', 'required|trim'], 
	        ['alias', 'Alias', 'required|alpha_dash|trim'], 
	        ['kode', 'Kode', 'required|trim|alpha_numeric'] 
	      ]); 
	 
	      if($validasi){ 
	        $data = [ 
	          'kode'         => strtoupper(post('kode')), 
	          'nama'        => post('nama'), 
	          'alias'        => post('alias'), 
	          'is_admin'      => (post('is_admin')) ? 1 : 0, 
	          'sys_branches_id'  => 1 
	        ]; 
	 
	        // insert data 
	        $ops = $this->M_role->insert(null, $data); 
	        if($ops){ 
	          $output = ['status' => true, 'msg' => "Data Berhasil Disimpan."]; 
	        } 
	      }else{ 
	        $output['msg'] = implode(" ", validation_errors_array()); 
	      } 
	 
	      echo json_encode($output); 
	    }else{ 
	      // redirect ke main page 
	      redirect($this->module_url); 
	    } 
	  } 
 
  	public function edit($id = null){ 
	    $id_raw = encrypt_decrypt('decrypt', $id); 
	 
	    if($this->_ajax_request_validator(self::HTTP_TYPE_POST) && $id_raw){ 
	      $output = ['status' => false]; 
	 
	      // cek validasi form 
	      $validasi = validation([ 
	        ['nama', 'Nama', 'required|trim'], 
	        ['alias', 'Alias', 'required|alpha_dash|trim'], 
	        ['kode', 'Kode', 'required|trim|alpha_numeric'] 
	      ]); 
	 
	      if($validasi){ 
	        $data = [ 
	          'kode'         => strtoupper(post('kode')), 
	          'nama'        => post('nama'), 
	          'alias'        => post('alias'), 
	          'is_admin'      => (post('is_admin')) ? 1 : 0, 
	          'sys_branches_id'  => 1 
	        ]; 
	 
	        // insert data 
	        $ops = $this->M_role->update(null, ['id' => $id_raw], $data); 
	        if($ops){ 
	          $output = ['status' => true, 'msg' => "Data Berhasil Disimpan."]; 
	        }else{ 
	          $output['msg'] = "Data Gagal Disimpan."; 
	        } 
	      }else{ 
	        $output['msg'] = implode(" ", validation_errors_array()); 
	      } 
	 
	      echo json_encode($output); 
	    }else{ 
	      // redirect ke main page 
	      redirect($this->module_url); 
	    } 
	  } 
	 
	  public function delete(){ 
	    $id_raw = encrypt_decrypt('decrypt', $this->input->input_stream('id')); 
	 
	    if($this->_ajax_request_validator(self::HTTP_TYPE_DELETE) && $id_raw){ 
	      $output = ['status' => false]; 
	 
	      $ops   = $this->M_role->delete(null, ['id' => $id_raw]); 
	      if($ops){ 
	        $output = ['status' => true, 'msg' => "Data Berhasil Dihapus."]; 
	      }else{ 
	        $output['msg'] = "Data Gagal Dihapus."; 
	      } 
	 
	      echo json_encode($output); 
	    }else{ 
	      // redirect ke main page 
	      redirect($this->module_url); 
	    } 
	  }
	  
	function _data_filter(){  
		$form = [  
		  'No. KK' => form_input(['name' => 'nokk', 'placeholder' => 'Nomor Kartu Keluarga'], null, 'class="form-control" data-live-search="true"'),
		  'NIK' => form_input(['name' => 'nik', 'placeholder' => 'Nomor Induk Kependudukan'], null, 'class="form-control" data-live-search="true"'),
		  'Provinsi' => form_dropdown('status', ['Role Normal', 'Super Administrator'], null, 'class="form-control selectpicker" data-live-search="true"'),
		  'Kota' => form_dropdown('status', ['Role Normal', 'Super Administrator'], null, 'class="form-control selectpicker" data-live-search="true"'),
		  'Kecamatan' => form_dropdown('status', ['Role Normal', 'Super Administrator'], null, 'class="form-control selectpicker" data-live-search="true"'),
		  'Kelurahan' => form_dropdown('status', ['Role Normal', 'Super Administrator'], null, 'class="form-control selectpicker" data-live-search="true"'),
		];  

		return $this->_filter_initialization($this->module_url, $form);  
	} 

	function _data_table(){  
		// inisialisasi tabel  
		$this->_table_initialization();  

		// header table  
		$this->table->set_heading(  
		        ['data' => 'No', 'class' => 'text-center', 'style' => 'width:8%;'],  
		      	['data' => 'NIK'],           
		      	['data' => 'Nama'],           
		        ['data' => 'Provinsi', 'class' => 'text-center'],  
		        ['data' => 'Kab. Kota', 'class' => 'text-center'],  
		        ['data' => 'Kecamatan', 'class' => 'text-center'],    
		        ['data' => 'Action', 'class' => 'text-center', 'style' => 'width:14%;']  
		);  
	  
		$action = [  
		    anchor($this->module_url.'#', '<i class="fa fa-eye fa-fw text-dark"></i>', 'class="detail-action" data-id="'.encrypt_decrypt('encrypt', '1').'"'),  
		    anchor($this->module_url.'#', '<i class="fa fa-edit fa-fw text-dark"></i>', 'class="update-action" data-id="'.encrypt_decrypt('encrypt', '1').'"'),  
		    anchor($this->module_url.'#', '<i class="fa fa-trash fa-fw text-danger"></i>', 'class="delete-action" data-id="'.encrypt_decrypt('encrypt', '1').'"')  
		  ];

		$this->table->add_row(  
		            ['data' => '1', 'class' => 'text-center'],  
				    ['data' => "7372122476547876"],  
				    ['data' => "Didot"],  
		            ['data' => 'Sulawesi Selatan', 'class' => 'text-center'],  
		            ['data' => 'Makassar', 'class' => 'text-center'],  
		            ['data' => 'Kampung Barru', 'class' => 'text-center'],
		            ['data' => implode('&nbsp;', $action), 'class' => 'text-center']  
		        );

		// dapatkan data  
		// $db_data = $this->M_role->get();  
		// foreach($db_data as $key => $row){  
		//   // is admin?  
		//   $is_admin = ($row->is_admin == 1) ? "<strong class='text-danger'>Super Admin</strong>" : "Role Normal";  

		//   // tombol action  
		//   $action = [  
		//     anchor($this->module_url.'#', '<i class="fa fa-edit fa-fw text-dark"></i>', 'class="update-action" data-id="'.encrypt_decrypt('encrypt', $row->id).'"'),  
		//     anchor($this->module_url.'#', '<i class="fa fa-trash fa-fw text-danger"></i>', 'class="delete-action" data-id="'.encrypt_decrypt('encrypt', $row->id).'"')  
		//   ];  

		//   $this->table->add_row(  
		//             ['data' => ++$key, 'class' => 'text-center'],  
		// 		    ['data' => "<h5 class='m-b-0 m-t-0'>{$row->nama}</h5><span>Kode: {$row->kode} &middot; Alias: {$row->alias}</span>"],  
		//             ['data' => $is_admin, 'class' => 'text-center'],  
		//             ['data' => implode('&nbsp;', $action), 'class' => 'text-center']  
		//         );  
	// }  

	return $this->table->generate();  
	}

	// TODO: JAVDOC
	function save(){
		if(! post()){
			$output = ["status" => false];
		}else{
			$validasi = validation([
				['code', 'Code', 'required|trim'],
				['nama', 'Nama Role', 'required|trim'],
				['alias', 'Nama Alias', 'required|trim'],
				['sys_branches', 'Branch', 'required|trim'],
			]);
			if($validasi){
				// bila inputan valid
				// array data yg akan diinput
				$data = array(
					'kode' => post('code'),
					'nama' => post('nama'),
					'alias' => post('alias'),
					'is_admin' => post('is_admin'),
					'sys_branches_id' => post('sys_branches')
				);
				// jika aksi merupakan tambah
				if (post('aksi')=='tambah') {
					// $this->M_role->insert(null,$data);
				}
				elseif (post('aksi')=='ubah') {
					// jika aksi merupakan ubah
					// $this->M_role->insert(null,['id' => post('id') ],$data);
				}
				$json = false;
			}else{
				// bila tidak, kembalikan ke halaman sebelumnya
				$json = validation_errors_array();
			}

			$output     = ["status" => true, "error" => $json ];
		}
		echo json_encode($output);	
	}
}
