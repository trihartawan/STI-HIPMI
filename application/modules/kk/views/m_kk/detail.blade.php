<div class="text-center">
  <h2 class="m-t-1 m-b-0">KARTU KELUARGA</h2>
  <p class="m-t-0 font-size-20">No. 73746688882990002</p>
</div>
<div class="detail row">
  <div class="col-md-7">
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1">Nama Kepala Keluarga</label>
      <label class="col-md-1">:</label>
      <label class="col-md-6">Dodit</label>  
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">Alamat</label>
      <label class="col-md-1 ">:</label>  
      <label class="col-md-6">Jln. </label>
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">RT/RW</label>
      <label class="col-md-1 ">:</label>  
      <label class="col-md-6">05/06</label>
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">Kode Pos</label>
      <label class="col-md-1 ">:</label>
      <label class="col-md-6">91111</label>  
    </div>
  </div>
  <div class="col-md-4 col-md-offset-1">
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">Desa/Kelurahan</label>
      <label class="col-md-1 ">:</label>
      <label class="col-md-6">Cabecabe</label>  
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">Kecamatan</label>
      <label class="col-md-1 ">:</label>
      <label class="col-md-6">Kampung Baru</label>  
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">Kabupaten</label>
      <label class="col-md-1 ">:</label>
      <label class="col-md-6">Makassar</label>  
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">Provinsi</label>
      <label class="col-md-1 ">:</label>
      <label class="col-md-6">Sulawesi Selatan</label>  
    </div>
  </div>
</div>
<div class="panel  m-t-3">
  <div class="panel-heading">
    <strong class="pull-xs-left">
        <span class="text-dark font-size-14">Daftar Anggota Keluarga</span>
    </strong>
    <div class="panel-tools">
        <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
  </div>
  <div class="panel-body">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center" style="width: 3%">No</th>
            <th class="text-center">Nama Lengkap</th>
            <th class="text-center">NIK</th>
            <th class="text-center" style="width: 8%">Jenis Kelamin</th>
            <th class="text-center">Tempat Lahir</th>
            <th class="text-center" style="width: 9%">Tanggal Lahir</th>
            <th class="text-center">Agama</th>
            <th class="text-center">Pendidikan</th>
            <th class="text-center">Jenis Pekerjaan</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Dodit</td>
            <td>7372xxxxxxxxxxxxx</td>
            <td>LAKI-LAKI</td>
            <td>Jakarta</td>
            <td>18-10-1997</td>
            <td>Islam</td>
            <td>SMA</td>
            <td>Siswa</td>
          </tr>
        </tbody>
      </table>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center" style="width: 3%">No</th>
            <th class="text-center" style="width: 10%">Status Perkawinan</th>
            <th class="text-center">NIK</th>
            <th class="text-center" style="width: 8%">Jenis Kelamin</th>
            <th class="text-center">Tempat Lahir</th>
            <th class="text-center" style="width: 9%">Tanggal Lahir</th>
            <th class="text-center">Agama</th>
            <th class="text-center">Pendidikan</th>
            <th class="text-center">Jenis Pekerjaan</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Dodit</td>
            <td>7372xxxxxxxxxxxxx</td>
            <td>LAKI-LAKI</td>
            <td>Jakarta</td>
            <td>18-10-1997</td>
            <td>Islam</td>
            <td>SMA</td>
            <td>Siswa</td>
          </tr>
        </tbody>
      </table>
  </div>
</div>