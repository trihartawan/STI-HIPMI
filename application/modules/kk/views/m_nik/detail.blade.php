<div class="text-center">
  <h2 class="m-t-1 m-b-0">PROVINSI SULAWESI SELATAN<br>MAKASSAR</h2>
  <p class="m-t-0 font-size-20">NIK : 7324638762765676</p>
</div>
<div class="detail row">
  <div class="col-md-2">
    <img src="{{ config('bootstrap', 'assets_dir') }}img/avatars/default.png" style="width: 150px">
    <div class="m-t-2" style="width: 150px; height: 100px;border: solid 1px #000;"></div>
  </div>
  <div class="col-md-5">
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1">Nama</label>
      <label class="col-md-1">:</label>
      <label class="col-md-6">Dodit Purwanto</label>  
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">Tempat/Tgl Lahir</label>
      <label class="col-md-1 ">:</label>  
      <label class="col-md-6">Makassar, 28-02-1992</label>
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">Jenis Kelamin</label>
      <label class="col-md-1 ">:</label>  
      <label class="col-md-6">Laki-laki</label>
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">Agama</label>
      <label class="col-md-1 ">:</label>
      <label class="col-md-6">Islam</label>  
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">Pekerjaan</label>
      <label class="col-md-1 ">:</label>
      <label class="col-md-6">Wirasuasta</label>  
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1">Alamat</label>
      <label class="col-md-1">:</label>
      <label class="col-md-6">Dusun Kauman</label>  
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-3 col-md-offset-2 ">RT/RW</label>
      <label class="col-md-1 ">:</label>  
      <label class="col-md-6">002/005</label>
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-3 col-md-offset-2 ">Kel/Desa</label>
      <label class="col-md-1 ">:</label>  
      <label class="col-md-6">Kesesi</label>
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-3 col-md-offset-2 ">Kecamatan</label>
      <label class="col-md-1 ">:</label>
      <label class="col-md-6">Kesesi</label>  
    </div>
    <div class="form-group row">
      <div id="error" class="col-md-12"></div>
      <label class="col-md-4 col-md-offset-1 ">Kewarganegaraan</label>
      <label class="col-md-1 ">:</label>
      <label class="col-md-6">WNI</label>  
    </div>
  </div>
</div>