<form>
  <div class="row">
    <div class="col-md-8">
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">No. KK</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <select class="form-control select2-example" name="sys_branches" style="width: 100%" data-allow-clear="true">
            <option></option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">NIK</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <input class=" form-control" type="text" name="nama" @if(isset($form)) value="{{ $form[0]->nama }}" @endif>
        </div>
      </div>
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">Nama</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <input class=" form-control" type="text" name="nama" @if(isset($form)) value="{{ $form[0]->nama }}" @endif>
        </div>
      </div>
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">Jenis Kelamin</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <select class="form-control select2-example" name="sys_branches" style="width: 100%" data-allow-clear="true">
            <option></option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">Tempat Lahir</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <input class=" form-control" type="text" name="nama" @if(isset($form)) value="{{ $form[0]->nama }}" @endif>
        </div>
      </div>
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">Agama</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <input class=" form-control" type="text" name="nama" @if(isset($form)) value="{{ $form[0]->nama }}" @endif>
        </div>
      </div>
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">Pendidikan</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <select class="form-control select2-example" name="sys_branches" style="width: 100%" data-allow-clear="true">
            <option></option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <div id="error" class="col-md-12"></div>
        <label class="col-md-3 text-right">Pekerjaan</label>
        <label class="col-md-1 text-right">:</label>  
        <div class="col-md-8">
          <select class="form-control select2-example" name="sys_branches" style="width: 100%" data-allow-clear="true">
            <option></option>
          </select>
        </div>
      </div>
    </div>
    <div class="col-md-4">
       <p><b>Petunjuk :</b></p>
        <span>Lengkapi form yang diperlukan di sebelah untuk membuat data baru</span>
    </div>
  </div>
</form>