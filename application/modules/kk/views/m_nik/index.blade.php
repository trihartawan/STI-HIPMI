@extends('layouts.adminweb.master')

@section('header_icon',' fa fa-users')

@section('content')
<div class="module-content" data-module-item="filter">
    
</div>
<div class="panel">
    <div class="panel-heading">
        <strong class="pull-xs-left">
            <span class="text-dark module-title font-size-18">Fetching...</span>
        </strong>
        <span class="pull-xs-right">
            <button type="button" class="btn btn-primary back-action"><i class="fa fa-arrow-left"></i> Kembali</button>
            <button type="button" class="btn btn-success create-action"><i class="fa fa-plus"></i> Tambah Data</button>
            <button type="button" class="btn btn-primary import-action"><i class="fa fa-file"></i> Import Data</button>
        </span>
    </div>
    <div class="panel-body">
        
        <div class="col-lg-12 module-content" data-module-item="table">
            
        </div>
        <div class="col-xs-12 module-spinner"></div>
    </div>    
</div>
@endsection

@section('custom_css')
<style>
    .detail .form-group{
        margin-bottom: 0px;
    }
</style>
@endsection

@section('custom_js')
  <script>
    // load form dan filter
    var module_url      = '{{ $url }}';  
    var back   = $('.back-action'); 
    var module_title    = $('.module-title'); 
    var module_spinner  = $('.module-spinner');
    var create          = $('.create-action');
    var a_import          = $('.import-action');
    var module_content  = $('.module-content'); 
    var module_table    = $('.module-content[data-module-item="table"]'); 
    var module_filter   = $('.module-content[data-module-item="filter"]'); 

    $(document).ready(function(){ 
        mainPage(); 
    });

    function mainPage(){
        url = module_url + "/ajax_get_page";
        setting = {
            content : module_content,
            spinner : module_spinner
        }
        showAjax(url,"GET",setting,{id : ''},module_url).done(function(data){
            module_filter.html(data.filter);
            module_table.html(data.table);
            module_table.find("#datatables").dataTable();
            $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
            back.addClass('display-none');
            module_title.text(data.title);
            minimazeCard();
        });   
    }

    $(document).on('click', '.back-action',function(){
        create.removeClass('display-none');
        a_import.removeClass('display-none');

        mainPage();
    });

    $(document).on('click', '.create-action',function(){
        url = module_url + "/ajax_get_form";
        setting = {
            content : module_content,
            spinner : module_spinner
        }
        showAjax(url,"GET",setting,{form_type: 'add', id : ''},url + "/tambah").done(function(data){
            module_filter.html(data.filter);
            module_table.html(data.table);
            back.removeClass('display-none');
            create.addClass('display-none');
            a_import.addClass('display-none');
            module_title.text(data.title);
        });
    });

    $(document).on('click', '.detail-action',function(){
        url = module_url + "/ajax_get_form";
        setting = {
            content : module_content,
            spinner : module_spinner
        }
        showAjax(url,"GET",setting,{form_type: 'detail', id : ''},module_url).done(function(data){
            module_filter.html(data.filter);
            module_table.html(data.table);
            back.removeClass('display-none');
            create.addClass('display-none');
            a_import.addClass('display-none');
            module_title.text(data.title);
            minimazeCard();
        }); 
    });

    $(document).on('click', '.update-action',function(){
        data_id = $(this).data('id');
        url = module_url + "/datapreview";
        setting = {
            content : module_content,
            spinner : module_spinner
        }
        showAjax(url,"GET",setting,{aksi: 'edit',id : data_id},url + "/edit/" + data_id).done(function(data){
            module_filter.html(data.filter);
            module_table.html(data.preview);
            back.removeClass('display-none');
            create.addClass('display-none');
            module_title.text(data.title);
        });
    });
    // crudAjax('#form',"{{ base_url('mgmt/role/') }}","#main-form",'#modal','Role');
    // modalAjax('#form',endPoint+'data','#modal','Role');
    
  </script>
@endsection
