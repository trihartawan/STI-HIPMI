<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends STI_Controller {

	/** Constructor */
	function __construct(){
		parent::__construct();
	}
	// TODO: JAVDOC
	public function index(){
		$data['title'] = "Email";
		$this->slice->view('data.index', $data);
	}

	public function messages(){
		$data['title'] = "Email Messages";
		$this->slice->view('data.messages', $data);
	}
}
