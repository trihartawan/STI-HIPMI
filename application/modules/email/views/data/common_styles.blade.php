<style type="text/css">
  /* Common styles */

  .box, .box-row, .box-cell { overflow: visible !important; -webkit-mask-image: none !important; }
  .page-messages-container > .box-row > .box-cell { display: block !important; }

  .page-messages-label {
    width: 8px;
    height: 8px;
    display: block;
    border-radius: 999px;
    float: left;
    margin-top: 6px;
    margin-right: 12px;
  }

  html[dir="rtl"] .page-messages-label {
    float: right;
    margin-left: 12px;
    margin-right: 0;
  }

  #page-messages-aside-nav {
    max-height: 0;
    overflow: hidden;
    -webkit-transition: max-height .3s;
    transition: max-height .3s;
  }

  #page-messages-aside-nav.show { max-height: 2000px; }

  @media (min-width: 768px) {
    .page-messages-container > .box-row > .box-cell {
      display: table-cell !important;
      padding-top: 15px;
    }
    .page-messages-aside { width: 200px; }
    .page-messages-content { padding-left: 20px; }

    html[dir="rtl"] .page-messages-content {
      padding-left: 0;
      padding-right: 20px;
    }

    #page-messages-aside-nav { max-height: none !important; }

    .page-messages-wide-buttons .btn { width: 60px; }
  }
</style>