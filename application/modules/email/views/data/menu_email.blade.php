<div class="page-messages-container box m-a-0 bg-transparent">
  <div class="box-row">

    <!-- Aside -->

    <div class="page-messages-aside box-cell valign-top">
      <div class="clearfix">

        <!-- Controls -->

        <div class="box-container">
          <div class="box-row">
            <div class="box-cell"><a onclick="openmodal(vaksi='tambah')" class="btn btn-block btn-primary"><i style="padding-bottom: 4%; padding-top: 5%" class="btn-label-icon left fa fa-pencil-square-o"></i>New Message</a></div>
            <div class="box-cell p-l-3 hidden-md hidden-lg hidden-xl" style="width: 60px;"><button type="button" class="btn btn-block btn-outline btn-outline-colorless p-x-0" id="page-messages-aside-nav-toggle"><i class="fa fa-bars"></i></button></div>
          </div>
        </div>

        <!-- / Controls -->

        <div class="m-t-3" id="page-messages-aside-nav">
          <div class="list-group m-b-1">
            <a href="#" class="list-group-item"><i class="list-group-icon fa fa-inbox"></i>Inbox<span class="label">20</span></a>
            <a href="#" class="list-group-item"><i class="list-group-icon fa fa-star"></i>Starred<span class="label">43</span></a>
            <a href="#" class="list-group-item"><i class="list-group-icon fa fa-envelope"></i>Sent</a>
            <a href="#" class="list-group-item"><i class="list-group-icon fa fa-exclamation"></i>Important</a>
            <a href="#" class="list-group-item"><i class="list-group-icon fa fa-file-text-o"></i>Drafts<span class="label">11</span></a>
            <a href="#" class="list-group-item"><i class="list-group-icon fa fa-trash-o"></i>Trash</a>
          </div>
          <a href="#" class="text-muted">+ Add More</a>

          <h6 class="m-t-4 m-b-1 text-muted">LABELS</h6>
          <div class="list-group m-b-1">
            <a href="#" class="list-group-item"><i class="page-messages-label bg-success"></i>Client</a>
            <a href="#" class="list-group-item"><i class="page-messages-label bg-danger"></i>Social</a>
            <a href="#" class="list-group-item"><i class="page-messages-label bg-info"></i>Family</a>
            <a href="#" class="list-group-item"><i class="page-messages-label bg-warning"></i>Friends</a>
          </div>
          <a href="#" class="text-muted">+ Add More</a>

          <!-- Spacer -->
          <div class="m-t-3 visible-xs visible-sm"></div>
        </div>

      </div>
    </div>

    <!-- / Aside -->

    <hr class="page-wide-block m-t-0 visible-xs visible-sm">