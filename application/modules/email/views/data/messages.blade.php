@extends('layouts.adminweb.master')

@section('header_icon',' fa fa-envelope')

@section('content')
  @include('email.data.menu_email')
    <div class="page-messages-content box-cell valign-top">
      <div class="panel">
        <div class="panel-title">
          <a href="#" class="pull-xs-left m-r-1 text-muted font-size-14"><i class="fa fa-star"></i></a>
          <span class="font-size-13"><span class="page-messages-item-label label label-success">Client</span></span>&nbsp;&nbsp;&nbsp;New design concepts <i class="fa fa-paperclip"></i>
        </div>

        <hr class="m-y-0">

        <!-- Controls -->

        <div class="panel-body p-y-1 clearfix">
          <div class="btn-toolbar page-messages-wide-buttons pull-left" role="toolbar">
            <div class="btn-group">
              <button type="button" class="btn"><i class="fa fa-chevron-left"></i></button>
            </div>

            <div class="btn-group">
              <button type="button" class="btn"><i class="fa fa fa-file-text-o"></i></button>
              <button type="button" class="btn"><i class="fa fa-exclamation-circle"></i></button>
              <button type="button" class="btn"><i class="fa fa-trash-o"></i></button>
            </div>
          </div>

          <div class="btn-toolbar pull-right" role="toolbar">
            <div class="btn-group">
              <button type="button" class="btn"><i class="fa fa-mail-reply"></i></button>
              <div class="btn-group">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"></button>
                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                  <li><a href="#"><i class="fa fa-mail-reply text-muted"></i>&nbsp;&nbsp;Reply</a></li>
                  <li><a href="#"><i class="fa fa-mail-forward text-muted"></i>&nbsp;&nbsp;Forward</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <!-- / Controls -->

        <hr class="m-y-0">

        <!-- Info -->

        <div class="panel-body p-y-1 clearfix">
          <div class="box m-a-0 valign-middle">
            <div class="box-cell col-md-8">
              <div class="box-container">
                <div class="box-row">
                  <div class="box-cell" style="width: 40px;">
                    <img src="{{ config('bootstrap', 'assets_back') }}img/avatars/default.png" alt="" class="border-round" style="width: 100%;">
                  </div>
                  <div class="box-cell p-l-2">
                    <div class="font-size-14">Michelle Bortz</div>
                    <div class="font-size-12 text-muted">mbortz@example.com</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="box-cell text-muted col-md-4 text-md-right">
              <!-- Spacer -->
              <div class="m-t-2 visible-xs visible-sm"></div>

              August 11 (3 days ago)
            </div>
          </div>
        </div>

        <!-- / Info -->

        <hr class="m-y-0">

        <!-- Message -->

        <div class="panel-body font-size-14">
          <p>Hi John,</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <blockquote>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </blockquote>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>

        <!-- / Message -->

        <hr class="m-y-0">

        <!-- Message -->

        <div class="panel-body">
          <table class="table m-a-0 table-striped">
            <tbody>
              <tr>
                <td class="b-a-0 p-x-2">
                  <div class="pull-md-left">
                    <span class="label label-warning">HTML</span>&nbsp;&nbsp;&nbsp;<a href="#"><strong>index.html</strong></a>&nbsp;<span class="text-muted">(12.6 KB)</span>
                  </div>

                  <!-- Spacer -->
                  <div class="m-t-1 visible-xs visible-sm"></div>

                  <div class="pull-md-right">
                    <a href="#">Download</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">View</a>
                  </div>
                </td>
              </tr>

              <tr>
                <td class="b-a-0 p-x-2">
                  <div class="pull-md-left">
                    <span class="label label-info">CSS</span>&nbsp;&nbsp;&nbsp;<a href="#"><strong>style.css</strong></a>&nbsp;<span class="text-muted">(12.6 KB)</span>
                  </div>

                  <!-- Spacer -->
                  <div class="m-t-1 visible-xs visible-sm"></div>

                  <div class="pull-md-right">
                    <a href="#">Download</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">View</a>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <!-- / Message -->

        <hr class="m-y-0">

        <!-- Form -->

        <div class="panel-body">
          <form action="" class="expanding-input" id="page-messages-item-form">
            <textarea class="form-control expanding-input-control" rows="4" placeholder="Click here to Reply or Forward"></textarea>

            <div class="expanding-input-content">
              <button type="submit" class="btn btn-primary pull-md-right">Send message</button>
            </div>
          </form>
        </div>

        <!-- / Form -->

      </div>
    </div>
  </div>
</div>
@endsection

@section('modal')
  @include('email.data.modal')
@endsection

@section('custom_css')
  @include('email.data.common_styles')
<style>
  /* Special styles */

  .page-messages-item-label {
    vertical-align: text-bottom;
  }
</style>
@endsection

@section('custom_js')
  @include('email.data.js')
@endsection
