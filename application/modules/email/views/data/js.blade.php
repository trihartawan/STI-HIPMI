<script type="text/javascript">
  // -------------------------------------------------------------------------
  // Initialize page components

  $(function() {
    $('#page-messages-aside-nav-toggle').on('click', function() {
      $(this)[
        $('#page-messages-aside-nav').toggleClass('show').hasClass('show') ? 'addClass' : 'removeClass'
      ]('active');
    });

    $("#page-messages-new-to").select2({
      data: [ { id: 0, text: 'rjang@example.com' }, { id: 1, text: 'mbortz@example.com' }, { id: 2, text: 'towens@example.com' }, { id: 3, text: 'dsteiner@example.com' }, ],
      tags: true,
      tokenSeparators: [',', ' ']
    });

    $('#page-messages-new-text').summernote({
      height: 200,
      toolbar: [
        ['parastyle', ['style']],
        ['fontstyle', ['fontname', 'fontsize']],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['insert', ['picture', 'link', 'video', 'table', 'hr']],
        ['history', ['undo', 'redo']],
        ['misc', ['codeview', 'fullscreen']],
        ['help', ['help']]
      ],
    });
  });

  // -------------------------------------------------------------------------
  // Modal Initialize
  var vmodal = $("#modal");
  var title = vmodal.find(".modal-title");
  var form = $('#form');
  var aksi = form.find('[name="aksi"]');
  var verror = $(".form-group").find("#error");

  // -------------------------------------------------------------------------
  // Modal Open
  function openmodal(vaksi,id=null){

    form[0].reset();
    verror.html('');
    aksi.val(vaksi);
    // mengambil data dri database jika vaksi = edit
    if (vaksi=='edit') {
      title.html('Edit Module');
      // $.ajax({

      // });
      form.find('[name="module"]').val("General");
    }else {
      title.html('Kirim Email');
    }      
    vmodal.modal('show');
  }

  // -------------------------------------------------------------------------
  // Save Modal
  function save(){
    $.ajax({
      url : "{{ base_url('mgmt/module/save') }}",
      type : "POST",
      dataType: "JSON",
      data: form.serialize(),
      beforeSend: function(){
        // ini operasi apa yang akan dijalankan sebelum request ajax dikirim
        // biasanya untuk logging jika masih dalam development aplikasi
        console.log("Request ke method ajax_get_pesan PHP...");

      },
      success : function(data){
          // mengenai statusnya apabila berhasil atau gagal
          if(data.status){
              // bila request berhasil, tampilkan pesan di HTML
              // data_php disini sesuai dengan index key di array yang ada di method PHP
              // hapus semua error
              verror.html('');
             // terdapat validasi error atau tidak
             if(data.error){
              // jika terdapat error, tampilkan error
                $.each(data.error, function(key, value){
                    $('[name="'+key+'"]').parents(".form-group").find("#error").html('<label class="col-md-4"></label><p class="col-md-8 text-danger">'+value+'</p>');
                });
             }else{
              // jika tidak tutup modal
              vmodal.modal('hide');
             }
          }else{
              // apabila gagal didapatkan maka tampilkan pesan ke user agar dapat diketahui statusnya
              // tanpa perlu menunggu lama padahal proses sudah selesai.
              htmlGET.html('Gagal Mendapatkan Data. Silakan Coba Lagi.');
          }
      }
    });
  }
</script>