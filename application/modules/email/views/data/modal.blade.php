<div class="modal fade" id="modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<form class="col-md-12" id="form">
	        	<div class="form-group row">
              <div id="error" class="col-md-12"></div>
	        		<label class="col-md-2 text-right">Daftar Module</label>
	        		<label class="col-md-1 text-right">:</label>	
	        		<div class="col-md-8">
	        			<input class=" form-control" type="text">
	        		</div>
	        	</div>
            <div class="form-group row">
              <div id="error" class="col-md-12"></div>
              <label class="col-md-2 text-right">From</label>
              <label class="col-md-1 text-right">:</label>
              <div class="col-md-8">
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <div id="error" class="col-md-12"></div>
              <label class="col-md-2 text-right">To</label>
              <label class="col-md-1 text-right">:</label>
              <div class="col-md-8">
                <input type="text" class="form-control">
              </div>
            </div>

            <div class="form-group row">
              <div id="error" class="col-md-12"></div>
              <label class="col-md-2 text-right">Subject</label>
              <label class="col-md-1 text-right">:</label>
              <div class="col-md-8">
                <textarea class="form-control" id="page-messages-new-text" rows="8"></textarea>
              </div>
            </div>
	        </form>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="save()">Save</button>
      </div>
    </div>

  </div>
</div>