<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

require APPPATH."third_party/MX/Controller.php";
require "../vendor/autoload.php";

use Carbon\Carbon;

// master class core controller
class STI_Controller extends MX_Controller {

    public $back;

    /** Constructor */
    function __construct(){
        parent::__construct();
        
        // set timezone aplikasi
        date_default_timezone_set('Asia/Makassar');

        // set bahasa untuk carbon
        Carbon::setLocale(WEBSITE_LOCALE);

        // set history -1 url
        $this->back = $this->agent->referrer();
    }
}

// subclass core controller (AuthCheck)
class Background_Controller extends STI_Controller {

    protected $user_data = [];

    const HTTP_TYPE_POST    = "POST"; 
    const HTTP_TYPE_GET     = "GET"; 
    const HTTP_TYPE_DELETE  = "DELETE";

    /** Constructor */
    function __construct(){
        parent::__construct();

        $this->_check_session();
        $this->_user_detail_data();
    }

    /**
     * Cek session user yang mengakses halaman management
     * 
     * @return	mixed
     */
    function _check_session(){
        // cek apakah session utk mgmt ada?
        if(! session(BACKGROUND_SESSION)){
            // redirect ke halaman login
            redirect(base_url('login'));
        }else{
            $session            = session(BACKGROUND_SESSION);
            $this->user_data    = $session;
        }
    }

    /**
     * Mendapatkan data detail dari masing-masing user
     * 
     * @return	void
     */
    function _user_detail_data(){
        // dapatkan data dateil user berdasarkan tabelnya masing-masing
        if($detail      = $this->M_login->get($this->user_data['user_table'], ['kode' => $this->user_data['user_kode']])){
            // dapatkan data role
            if($role    = $this->M_role->get(null, ['id' => $this->user_data['user_roles_id']])){

                // bila gambar user kosong tampilkan default
                if(! $detail[0]->gambar) {
                    $gambar = config('bootstrap', 'assets_back')."img/avatars/default.png";
                }else{
                    $gambar = $detail[0]->gambar;
                }

                $data   = [
                    'user_code'     => $detail[0]->kode,
                    'user_fullname' => $detail[0]->nama,
                    'user_picture'  => $gambar,
                    'roles_nama'    => $role[0]->nama,
                    'roles_admin'   => $role[0]->is_admin,
                    'roles_alias'   => $role[0]->alias
                ];

                // gabung data detail dengan yang ada di dalam session
                $this->user_data = array_merge($this->user_data, $data);

                // masukkan dan update dalam session
                session([BACKGROUND_SESSION => $this->user_data]);
            }
        }
    }

    // TODO: JAVDOC
    function _table_initialization(){
        // untuk set default template tabel
        $template = array(
            'table_open'            => '<div class="table-responsive"><table class="table table-striped table-bordered datatable table-sm">',
            'thead_open'            => '<thead style="background:#EEE">',
            'thead_close'           => '</thead>',
            'heading_row_start'     => '<tr>',
            'heading_row_end'       => '</tr>',
            'heading_cell_start'    => '<th style="vertical-align:middle;">',
            'heading_cell_end'      => '</th>',
            'tbody_open'            => '<tbody>',
            'tbody_close'           => '</tbody>',
            'row_start'             => '<tr>',
            'row_end'               => '</tr>',
            'cell_start'            => '<td style="vertical-align:middle;">',
            'cell_end'              => '</td>',
            'row_alt_start'         => '<tr>',
            'row_alt_end'           => '</tr>',
            'cell_alt_start'        => '<td style="vertical-align:middle;">',
            'cell_alt_end'          => '</td>',    
            'table_close'           => '</table></div>'
        );
        
        $this->table->set_template($template);
    }

    // TODO: JAVDOC
    function _filter_initialization($url = '#', $form = []){
        // dapatkan data konten
        $child      = null;

        foreach($form as $key => $row){
            $child  = '<div class="form-group"><label>'.$key.'</label>'.$row.'</div>';
        }

        $html       = '<form method="GET" action="'.$url.'" class="form-horizontal m-b-40">
                    <h4 class="text-bold">Filter Data</h4>
                    <p>Gunakan form di bawah ini untuk mendapatkan data yang lebih sepesifik.</p>
                    '.$child.'
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="btn-group float-right">
                                <button type="submit" class="btn btn-info">Cari Data</button>
                            </div>
                        </div>
                    </div>
                </form>';
        
        return $html;
    }

    // TODO: JAVDOC
    function _ajax_request_validator($request_type = self::HTTP_TYPE_GET){
        // variable output
        $output = true;

        // cek apakah ini merupakan request ajax dan tipe methodnya sesuai
        if(! $this->input->is_ajax_request() && $this->input->method(TRUE) != $request_type){
            // bila tidak tampilkan halaman error
            $this->output->set_status_header(401);
            $output = false;
        }

        return $output;
    }
}

// subclass core controller (Non AuthCheck)
class Foreground_Controller extends STI_Controller {

    /** Constructor */
    function __construct(){
        parent::__construct();
    }
}