<?php


use Phinx\Seed\AbstractSeed;

class InitialSeeds extends AbstractSeed
{
    protected $faker;
    
    public function run()
    {
        $this->faker = Faker\Factory::create('id_ID');
        
        $this->sys_settings();
        $this->sys_companies();
        $this->sys_user_tables();
        $this->sys_branches();
        $this->sys_roles();
        $this->sys_divisions();
        $this->sys_users();
        $this->sys_notifications();
        $this->sys_user_workplaces();
        $this->master_karyawan();
    }

    function sys_settings(){
        $data = [
            [
                'itemset'   => 'NewUserRole',
                'val1'      => 1
            ],[
                'itemset'   => 'NewUserDivision',
                'val1'      => 1
            ]
        ];

        // masukkan data kedalam tabel        
        $seeds = $this->table('sys_settings');
        $seeds->insert($data)->save();
    }

    function sys_companies(){
        $data = [];

        for($i = 1; $i <= 2; $i++){
            $data[] = [
                'nama'      => $this->faker->company,
                'email'     => $this->faker->email,
                'website'   => $this->faker->domainName
            ];
        }

        // masukkan data kedalam tabel        
        $seeds = $this->table('sys_companies');
        $seeds->insert($data)->save();
    }

    function sys_user_tables(){
        $data = [
            'kode'  => 'mst_ky',
            'nama'  => 'Karyawan',
            'table' => 'master_karyawan'
        ];

        // masukkan data kedalam tabel        
        $seeds = $this->table('sys_user_tables');
        $seeds->insert($data)->save();
    }

    function sys_branches(){
        $data = [];

        // get company
        $fetch = $this->fetchAll('SELECT * FROM sys_companies');
        foreach($fetch as $key => $row){
            $id = $row['id'];

            for($i = 1; $i <= 2; $i++){
                $is_hq = ($i == 1) ? 1 : 0;

                $data[] = [
                    'kode'              => $this->faker->lexify('???'),
                    'nama'              => $this->faker->city,
                    'is_hq'             => $is_hq,
                    'alamat'            => $this->faker->address,
                    'telpon'            => $this->faker->e164PhoneNumber,
                    'kodepos'           => $this->faker->postcode,
                    'fax'               => $this->faker->e164PhoneNumber,
                    'npwp'              => $this->faker->randomNumber(),
                    'rekening'          => $this->faker->randomNumber(),
                    'sys_companies_id'  => $id
                ];
            }
        }

        // masukkan data kedalam tabel        
        $seeds = $this->table('sys_branches');
        $seeds->insert($data)->save();
    }

    function sys_roles(){
        $data = [];

        // get company
        $fetch = $this->fetchAll('SELECT * FROM sys_branches');
        foreach($fetch as $key => $row){
            $id = $row['id'];

            for($i = 1; $i <= 2; $i++){
                $is_admin   = ($i == 1) ? 1 : 0;
                $nama       = $this->faker->randomElement(['Administrator', 'User']);
                $data[] = [
                    'kode'              => $this->faker->lexify('???'),
                    'nama'              => $nama,
                    'alias'             => strtolower($nama),
                    'is_admin'          => $is_admin,
                    'sys_branches_id'   => $id
                ];
            }
        }

        // masukkan data kedalam tabel        
        $seeds = $this->table('sys_roles');
        $seeds->insert($data)->save();
    }

    function sys_users(){
        $data = [];

        // get company
        $fetch = $this->fetchAll('SELECT * FROM sys_roles');
        foreach($fetch as $key => $row){
            $id = $row['id'];

            for($i = 1; $i <= 2; $i++){
                $salt = $this->faker->password;
                if($i == 1){
                    // untuk administrator
                    $data[] = [
                        'kode'                  => 'ADM',
                        'email'                 => 'admin@sti.com',
                        'password'              => sha1("password".$salt),
                        'salt'                  => $salt,
                        'status'                => 1,
                        'sys_user_tables_id'    => 1,
                        'sys_roles_id'          => $id
                    ];
                }else{
                    $data[] = [
                        'kode'                  => $this->faker->lexify('???'),
                        'email'                 => $this->faker->email,
                        'password'              => sha1("password".$salt),
                        'salt'                  => $salt,
                        'status'                => 1,
                        'sys_user_tables_id'    => 1,
                        'sys_roles_id'          => $id
                    ];
                }
            }
        }

        // masukkan data kedalam tabel        
        $seeds = $this->table('sys_users');
        $seeds->insert($data)->save();
    }

    function sys_notifications(){
        $data = [];

        // get company
        $fetch = $this->fetchAll('SELECT * FROM sys_users');
        foreach($fetch as $key => $row){
            $id = $row['id'];

            for($i = 1; $i <= 3; $i++){
                
                $salt   = $this->faker->password;
                $data[] = [
                    'id'                    => sha1($this->faker->password),
                    'judul'                 => $this->faker->sentence(2),
                    'konten'                => $this->faker->sentence(),
                    'url'                   => '#',
                    'is_read'               => 0,
                    'sys_users_id'          => $id
                ];
            }
        }

        // masukkan data kedalam tabel        
        $seeds = $this->table('sys_notifications');
        $seeds->insert($data)->save();
    }

    function sys_user_workplaces(){
        $data = [];

        // get company
        $fetch = $this->fetchAll('SELECT * FROM sys_users');
        foreach($fetch as $key => $row){
            $id_user    = $row['id'];

            $division   = $this->fetchAll('SELECT * FROM sys_divisions');
            $branches   = $this->fetchAll('SELECT * FROM sys_branches');

            $t_division = count($division);
            $t_branches = count($branches);

            $data[] = [
                'sys_users_id'      => $id_user,
                'sys_divisions_id'  => rand(1, ($t_division - 1)),
                'sys_branches_id'   => rand(1, ($t_branches - 1))
            ];
        }

        // masukkan data kedalam tabel        
        $seeds = $this->table('sys_user_workplaces');
        $seeds->insert($data)->save();
    }

    function master_karyawan(){
        $data = [];

        // get company
        $fetch = $this->fetchAll('SELECT * FROM sys_users');
        foreach($fetch as $key => $row){
            $kode   = $row['kode'];

            $data[] = [
                'kode'          => $kode,
                'nama'          => $this->faker->name,
                'alamat'        => $this->faker->address,
                'telpon'        => $this->faker->phoneNumber,
                'hp'            => $this->faker->e164PhoneNumber,
                'profil'        => $this->faker->paragraph(5),
                'tggl_lahir'    => $this->faker->date()
            ];
        }

        // masukkan data kedalam tabel        
        $seeds = $this->table('master_karyawan');
        $seeds->insert($data)->save();
    }

    function sys_divisions(){
        $data = [
            [
                'kode'  => 'DDU',
                'nama'  => 'Direktorat Utama',
            ],[
                'kode'  => 'DKU',
                'nama'  => 'Keuangan',
            ]
        ];

        // masukkan data kedalam tabel        
        $seeds = $this->table('sys_divisions');
        $seeds->insert($data)->save();
    }
}
