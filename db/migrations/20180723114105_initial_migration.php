<?php


use Phinx\Migration\AbstractMigration;

class InitialMigration extends AbstractMigration
{
    public function change()
    {   
        $this->sys_settings();
        $this->sys_companies();
        $this->sys_divisions();
        $this->sys_user_tables();
        $this->sys_modules();
        $this->sys_branches();
        $this->sys_roles();
        $this->sys_users();
        $this->sys_sessions();
        $this->sys_confirmations();
        $this->sys_notifications();
        $this->sys_user_workplaces();
        $this->sys_access();
        $this->sys_permissions();
        $this->sys_menus();
        $this->sys_menu_roles();
        $this->master_karyawan();
    }

    function sys_settings(){
        $table = $this->table('sys_settings', ['id' => false, 'primary_key' => 'itemset', 'engine' => 'InnoDB']);
        $table
            ->addColumn('itemset', 'string', ['limit' => 250, 'null' => false])
            ->addColumn('val1', 'text', ['null' => true])
            ->addColumn('val2', 'text', ['null' => true])
            ->addColumn('val3', 'text', ['null' => true])
            ->addColumn('val4', 'text', ['null' => true])
            ->addColumn('val5', 'text', ['null' => true])
            ->addColumn('val6', 'text', ['null' => true]);
			
		// execute query			
		$table->create();
    }

    function sys_companies(){
        $table = $this->table('sys_companies', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('nama', 'string', ['limit' => 100, 'null' => false])
            ->addColumn('email', 'string', ['limit' => 80, 'null' => false])
            ->addColumn('website', 'string', ['limit' => 80, 'null' => false]);

        $table->addTimestamps();
			
		// execute query			
		$table->create();
    }

    function sys_divisions(){
        $table = $this->table('sys_divisions', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('kode', 'string', ['limit' => 40, 'null' => false])
            ->addColumn('nama', 'string', ['limit' => 100, 'null' => false])
            ->addColumn('parent_id', 'biginteger', ['limit' => 20, 'null' => true])
            ->addIndex(['parent_id'])
            ->addForeignKey('parent_id', 'sys_divisions', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]);
			
		// execute query			
		$table->create();
    }

    function sys_user_tables(){
        $table = $this->table('sys_user_tables', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('kode', 'string', ['limit' => 40, 'null' => false])
            ->addColumn('nama', 'string', ['limit' => 100, 'null' => false])
            ->addColumn('table', 'string', ['limit' => 60, 'null' => false]);
			
		// execute query			
		$table->create();
    }

    function sys_modules(){
        $table = $this->table('sys_modules', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'string', ['limit' => 80, 'null' => false])
            ->addColumn('nama', 'string', ['limit' => 100, 'null' => false])
            ->addColumn('status', 'integer', ['limit' => 1, 'default' => 1])
            ->addColumn('versi', 'string', ['limit' => 10, 'default' => '1.0.0'])
            ->addColumn('tipe', 'enum', ['values' => ['Sys', 'App'], 'default' => 'App', 'null' => false]);
        
        $table->addTimestamps();
			
		// execute query			
		$table->create();
    }

    function sys_branches(){
        $table = $this->table('sys_branches', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('kode', 'string', ['limit' => 40, 'null' => false])
            ->addColumn('nama', 'string', ['limit' => 100])
            ->addColumn('is_hq', 'integer', ['limit' => 1, 'default' => 1])
            ->addColumn('alamat', 'text')
            ->addColumn('telpon', 'string', ['limit' => 20])
            ->addColumn('negara', 'string', ['limit' => 30, 'default' => 'Indonesia'])
            ->addColumn('kodepos', 'string', ['limit' => 6])
            ->addColumn('fax', 'string', ['limit' => 20])
            ->addColumn('npwp', 'string', ['limit' => 40])
            ->addColumn('rekening', 'string', ['limit' => 50])
            ->addColumn('sys_companies_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addIndex(['sys_companies_id'])
            ->addForeignKey('sys_companies_id', 'sys_companies', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]);
        
        $table->addTimestamps();
			
		// execute query			
		$table->create();
    }

    function sys_roles(){
        $table = $this->table('sys_roles', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('kode', 'string', ['limit' => 40, 'null' => false])
            ->addColumn('nama', 'string', ['limit' => 100, 'null' => false])
            ->addColumn('alias', 'string', ['limit' => 30, 'null' => false])
            ->addColumn('is_admin', 'integer', ['limit' => 1, 'default' => 0])
            ->addColumn('sys_branches_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addIndex(['sys_branches_id'])
            ->addForeignKey('sys_branches_id', 'sys_branches', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]);

		// execute query			
		$table->create();
    }

    function sys_users(){
        $table = $this->table('sys_users', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('kode', 'string', ['limit' => 40, 'null' => false])
            ->addColumn('email', 'string', ['limit' => 80])
            ->addColumn('password', 'string', ['limit' => 200, 'null' => false])
            ->addColumn('salt', 'string', ['limit' => 200])
            ->addColumn('status', 'integer', ['limit' => 1, 'null' => false, 'default' => 1])
            ->addColumn('sys_roles_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addColumn('sys_user_tables_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addIndex(['sys_roles_id'])
            ->addIndex(['sys_user_tables_id'])
            ->addForeignKey('sys_roles_id', 'sys_roles', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ])
            ->addForeignKey('sys_user_tables_id', 'sys_user_tables', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]);

        $table->addTimestamps();        

		// execute query			
		$table->create();
    }

    function sys_sessions(){
        $table = $this->table('sys_sessions', ['id' => false, 'primary_key' => 'token', 'engine' => 'InnoDB']);
        $table
            ->addColumn('token', 'string', ['limit' => 80, 'null' => false])
            ->addColumn('user_agent', 'text')
            ->addColumn('ip_address', 'string', ['limit' => 200])
            ->addColumn('sys_users_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addIndex(['sys_users_id'])
            ->addForeignKey('sys_users_id', 'sys_users', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]);

        $table->addTimestamps();        

		// execute query			
		$table->create();
    }

    function sys_confirmations(){
        $table = $this->table('sys_confirmations', ['id' => false, 'primary_key' => 'token', 'engine' => 'InnoDB']);
        $table
            ->addColumn('token', 'string', ['limit' => 80, 'null' => false])
            ->addColumn('tipe', 'enum', ['values' => ['F', 'C'], 'default' => 'F'])
            ->addColumn('tggl_expired', 'datetime')
            ->addColumn('sys_users_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addIndex(['sys_users_id'])
            ->addForeignKey('sys_users_id', 'sys_users', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]);

		// execute query			
		$table->create();
    }

    function sys_notifications(){
        $table = $this->table('sys_notifications', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'string', ['limit' => 100, 'null' => false])
            ->addColumn('judul', 'string', ['limit' => 120, 'null' => false, 'default' => 'Notifikasi'])
            ->addColumn('konten', 'string', ['limit' => 600])
            ->addColumn('url', 'string', ['limit' => 300, 'default' => '#'])
            ->addColumn('is_read', 'integer', ['limit' => 1, 'default' => 0])
            ->addColumn('sys_users_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addIndex(['sys_users_id'])
            ->addForeignKey('sys_users_id', 'sys_users', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]);

        $table->addTimestamps();        

		// execute query			
		$table->create();
    }

    function sys_user_workplaces(){
        $table = $this->table('sys_user_workplaces', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('sys_users_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addColumn('sys_divisions_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addColumn('sys_branches_id', 'biginteger', ['limit' => 20, 'null' => false])  
            ->addIndex(['sys_users_id'])
            ->addIndex(['sys_divisions_id'])
            ->addIndex(['sys_branches_id'])
            ->addForeignKey('sys_users_id', 'sys_users', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ])
            ->addForeignKey('sys_divisions_id', 'sys_divisions', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ])
            ->addForeignKey('sys_branches_id', 'sys_branches', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]);

		// execute query			
		$table->create();
    }

    function sys_access(){
        $table = $this->table('sys_access', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('sys_modules_id', 'string', ['limit' => 80, 'null' => false])
            ->addColumn('nama_class', 'string', ['limit' => 80, 'null' => false])
            ->addColumn('nama_access', 'string', ['limit' => 80, 'null' => false])
            ->addIndex(['sys_modules_id'])
            ->addForeignKey('sys_modules_id', 'sys_modules', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]); 

		// execute query			
		$table->create();
    }

    function sys_permissions(){
        $table = $this->table('sys_permissions', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('sys_access_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addColumn('sys_roles_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addColumn('add', 'integer', ['limit' => 1, 'null' => false, 'default' => 0])
            ->addColumn('delete', 'integer', ['limit' => 1, 'null' => false, 'default' => 0])
            ->addColumn('edit', 'integer', ['limit' => 1, 'null' => false, 'default' => 0])
            ->addColumn('detail', 'integer', ['limit' => 1, 'null' => false, 'default' => 0])
            ->addColumn('download', 'integer', ['limit' => 1, 'null' => false, 'default' => 0])
            ->addIndex(['sys_access_id'])
            ->addIndex(['sys_roles_id'])
            ->addForeignKey('sys_access_id', 'sys_access', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ])
            ->addForeignKey('sys_roles_id', 'sys_roles', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]);

		// execute query			
		$table->create();
    }

    function sys_menus(){
        $table = $this->table('sys_menus', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('judul', 'string', ['limit' => 120, 'null' => false, 'default' => 'Notifikasi'])
            ->addColumn('url', 'string', ['limit' => 300, 'default' => '#'])
            ->addColumn('tipe', 'enum', ['values' => ['F', 'B'], 'default' => 'B', 'comment' => 'F: Frontend, B: Management'])
            ->addColumn('sys_access_id', 'biginteger', ['limit' => 20, 'null' => true])
            ->addIndex(['sys_access_id'])
            ->addForeignKey('sys_access_id', 'sys_access', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]);

		// execute query			
		$table->create();
    }

    function sys_menu_roles(){
        $table = $this->table('sys_menu_roles', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('urutan', 'integer', ['limit' => 5, 'null' => false, 'default' => 0])
            ->addColumn('sys_roles_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addColumn('sys_menus_id', 'biginteger', ['limit' => 20, 'null' => false])
            ->addIndex(['sys_roles_id'])
            ->addIndex(['sys_menus_id'])
            ->addForeignKey('sys_roles_id', 'sys_roles', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ])
            ->addForeignKey('sys_menus_id', 'sys_menus', 'id', 
                [
                    'delete'    => 'CASCADE', 
                    'update'    => 'CASCADE'
                ]);

		// execute query			
		$table->create();
    }

    function master_karyawan(){
        $table = $this->table('master_karyawan', ['id' => false, 'primary_key' => 'id', 'engine' => 'InnoDB']);
        $table
            ->addColumn('id', 'biginteger', ['limit' => 20, 'null' => false, 'identity' => true])
            ->addColumn('kode', 'string', ['limit' => 40, 'null' => false])
            ->addColumn('nama', 'string', ['limit' => 80])
            ->addColumn('alamat', 'text')
            ->addColumn('telpon', 'string', ['limit' => 20])
            ->addColumn('hp', 'string', ['limit' => 20])
            ->addColumn('profil', 'text')
            ->addColumn('gambar', 'text', ['null' => true])
            ->addColumn('tggl_lahir', 'date')
            ->addIndex(['kode']);

        $table->addTimestamps(); 

		// execute query			
		$table->create();
    }
}