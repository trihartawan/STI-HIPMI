<?php


use Phinx\Migration\AbstractMigration;

class DelConfrimationTable extends AbstractMigration
{
    public function change(){
        $this->sys_confirmations();
    }

    function sys_confirmations(){
        $this->table('sys_confirmations')->drop()->save();
    }
}
